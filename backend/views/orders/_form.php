<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Orders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'USER_ID')->textInput() ?>

    <?= $form->field($model, 'ZAVEDENIYE_ID')->textInput() ?>

    <?= $form->field($model, 'TRANSACTION_ID')->textInput() ?>

    <?= $form->field($model, 'MAIL_SENT')->textInput() ?>

    <?= $form->field($model, 'SMS_SENT')->textInput() ?>

    <?= $form->field($model, 'PAID')->textInput() ?>

    <?= $form->field($model, 'CUSTOM')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DATE')->textInput() ?>

    <?= $form->field($model, 'ORDER_COMMENT')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'ORDER_DISCOUNT')->textInput() ?>

    <?= $form->field($model, 'ORDER_DISCOUNT_TYPE')->textInput() ?>

    <?= $form->field($model, 'ORDER_DATE')->textInput() ?>

    <?= $form->field($model, 'ORDER_TIME')->textInput() ?>

    <?= $form->field($model, 'ORDER_PERSONS')->textInput() ?>

    <?= $form->field($model, 'ORDER_KEY')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ORDER_AMT')->textInput() ?>

    <?= $form->field($model, 'CANCEL_DATE')->textInput() ?>

    <?= $form->field($model, 'STATUS')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'REF_URL')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'REF_NAME')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'REF_ID')->textInput() ?>

    <?= $form->field($model, 'IP')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CLIENT_LAT')->textInput() ?>

    <?= $form->field($model, 'CLIENT_LNG')->textInput() ?>

    <?= $form->field($model, 'CLIENT_CITY')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CLIENT_STREET')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CLIENT_REGION')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'IS_FB')->textInput() ?>

    <?= $form->field($model, 'IS_BANQUET')->textInput() ?>

    <?= $form->field($model, 'IS_DELETED')->textInput() ?>

    <?= $form->field($model, 'CARD_NUMBER')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'TABLE_NUM')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'TABLE_ID')->textInput() ?>

    <?= $form->field($model, 'MAP_ID')->textInput() ?>

    <?= $form->field($model, 'HOSTES_RESERV')->textInput() ?>

    <?= $form->field($model, 'MESSAGE_SET')->textInput() ?>

    <?= $form->field($model, 'IS_CANCELED')->textInput() ?>

    <?= $form->field($model, 'IN_PROCESS')->textInput() ?>

    <?= $form->field($model, 'CAME_DATE')->textInput() ?>

    <?= $form->field($model, 'CUSTOMER_NAME')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PREPAYMENT')->textInput() ?>

    <?= $form->field($model, 'CLOSE_DATE')->textInput() ?>

    <?= $form->field($model, 'PRECHECK_DATE')->textInput() ?>

    <?= $form->field($model, 'TMP_PARAM')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'IS_INTEGRATED')->textInput() ?>

    <?= $form->field($model, 'IS_CONFIRMED')->textInput() ?>

    <?= $form->field($model, 'MANAGER_ID')->textInput() ?>

    <?= $form->field($model, 'CONFIRM_SOURCE')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ZAL_ID')->textInput() ?>

    <?= $form->field($model, 'IS_RESTOWEEK')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
