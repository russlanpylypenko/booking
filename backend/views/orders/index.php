<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Orders', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'USER_ID',
            'ZAVEDENIYE_ID',
            'TRANSACTION_ID',
            'MAIL_SENT',
            //'SMS_SENT',
            //'PAID',
            //'CUSTOM',
            //'DATE',
            //'ORDER_COMMENT:ntext',
            //'ORDER_DISCOUNT',
            //'ORDER_DISCOUNT_TYPE',
            //'ORDER_DATE',
            //'ORDER_TIME',
            //'ORDER_PERSONS',
            //'ORDER_KEY',
            //'ORDER_AMT',
            //'CANCEL_DATE',
            //'STATUS',
            //'REF_URL:url',
            //'REF_NAME',
            //'REF_ID',
            //'IP',
            //'CLIENT_LAT',
            //'CLIENT_LNG',
            //'CLIENT_CITY',
            //'CLIENT_STREET',
            //'CLIENT_REGION',
            //'IS_FB',
            //'IS_BANQUET',
            //'IS_DELETED',
            //'CARD_NUMBER',
            //'TABLE_NUM',
            //'TABLE_ID',
            //'MAP_ID',
            //'HOSTES_RESERV',
            //'MESSAGE_SET',
            //'IS_CANCELED',
            //'IN_PROCESS',
            //'CAME_DATE',
            //'CUSTOMER_NAME',
            //'PREPAYMENT',
            //'CLOSE_DATE',
            //'PRECHECK_DATE',
            //'TMP_PARAM',
            //'IS_INTEGRATED',
            //'IS_CONFIRMED',
            //'MANAGER_ID',
            //'CONFIRM_SOURCE',
            //'ZAL_ID',
            //'IS_RESTOWEEK',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
