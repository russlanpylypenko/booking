<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\OrderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'USER_ID') ?>

    <?= $form->field($model, 'ZAVEDENIYE_ID') ?>

    <?= $form->field($model, 'TRANSACTION_ID') ?>

    <?= $form->field($model, 'MAIL_SENT') ?>

    <?php // echo $form->field($model, 'SMS_SENT') ?>

    <?php // echo $form->field($model, 'PAID') ?>

    <?php // echo $form->field($model, 'CUSTOM') ?>

    <?php // echo $form->field($model, 'DATE') ?>

    <?php // echo $form->field($model, 'ORDER_COMMENT') ?>

    <?php // echo $form->field($model, 'ORDER_DISCOUNT') ?>

    <?php // echo $form->field($model, 'ORDER_DISCOUNT_TYPE') ?>

    <?php // echo $form->field($model, 'ORDER_DATE') ?>

    <?php // echo $form->field($model, 'ORDER_TIME') ?>

    <?php // echo $form->field($model, 'ORDER_PERSONS') ?>

    <?php // echo $form->field($model, 'ORDER_KEY') ?>

    <?php // echo $form->field($model, 'ORDER_AMT') ?>

    <?php // echo $form->field($model, 'CANCEL_DATE') ?>

    <?php // echo $form->field($model, 'STATUS') ?>

    <?php // echo $form->field($model, 'REF_URL') ?>

    <?php // echo $form->field($model, 'REF_NAME') ?>

    <?php // echo $form->field($model, 'REF_ID') ?>

    <?php // echo $form->field($model, 'IP') ?>

    <?php // echo $form->field($model, 'CLIENT_LAT') ?>

    <?php // echo $form->field($model, 'CLIENT_LNG') ?>

    <?php // echo $form->field($model, 'CLIENT_CITY') ?>

    <?php // echo $form->field($model, 'CLIENT_STREET') ?>

    <?php // echo $form->field($model, 'CLIENT_REGION') ?>

    <?php // echo $form->field($model, 'IS_FB') ?>

    <?php // echo $form->field($model, 'IS_BANQUET') ?>

    <?php // echo $form->field($model, 'IS_DELETED') ?>

    <?php // echo $form->field($model, 'CARD_NUMBER') ?>

    <?php // echo $form->field($model, 'TABLE_NUM') ?>

    <?php // echo $form->field($model, 'TABLE_ID') ?>

    <?php // echo $form->field($model, 'MAP_ID') ?>

    <?php // echo $form->field($model, 'HOSTES_RESERV') ?>

    <?php // echo $form->field($model, 'MESSAGE_SET') ?>

    <?php // echo $form->field($model, 'IS_CANCELED') ?>

    <?php // echo $form->field($model, 'IN_PROCESS') ?>

    <?php // echo $form->field($model, 'CAME_DATE') ?>

    <?php // echo $form->field($model, 'CUSTOMER_NAME') ?>

    <?php // echo $form->field($model, 'PREPAYMENT') ?>

    <?php // echo $form->field($model, 'CLOSE_DATE') ?>

    <?php // echo $form->field($model, 'PRECHECK_DATE') ?>

    <?php // echo $form->field($model, 'TMP_PARAM') ?>

    <?php // echo $form->field($model, 'IS_INTEGRATED') ?>

    <?php // echo $form->field($model, 'IS_CONFIRMED') ?>

    <?php // echo $form->field($model, 'MANAGER_ID') ?>

    <?php // echo $form->field($model, 'CONFIRM_SOURCE') ?>

    <?php // echo $form->field($model, 'ZAL_ID') ?>

    <?php // echo $form->field($model, 'IS_RESTOWEEK') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
