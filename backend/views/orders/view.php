<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Orders */

$this->title = $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="orders-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID',
            'USER_ID',
            'ZAVEDENIYE_ID',
            'TRANSACTION_ID',
            'MAIL_SENT',
            'SMS_SENT',
            'PAID',
            'CUSTOM',
            'DATE',
            'ORDER_COMMENT:ntext',
            'ORDER_DISCOUNT',
            'ORDER_DISCOUNT_TYPE',
            'ORDER_DATE',
            'ORDER_TIME',
            'ORDER_PERSONS',
            'ORDER_KEY',
            'ORDER_AMT',
            'CANCEL_DATE',
            'STATUS',
            'REF_URL:url',
            'REF_NAME',
            'REF_ID',
            'IP',
            'CLIENT_LAT',
            'CLIENT_LNG',
            'CLIENT_CITY',
            'CLIENT_STREET',
            'CLIENT_REGION',
            'IS_FB',
            'IS_BANQUET',
            'IS_DELETED',
            'CARD_NUMBER',
            'TABLE_NUM',
            'TABLE_ID',
            'MAP_ID',
            'HOSTES_RESERV',
            'MESSAGE_SET',
            'IS_CANCELED',
            'IN_PROCESS',
            'CAME_DATE',
            'CUSTOMER_NAME',
            'PREPAYMENT',
            'CLOSE_DATE',
            'PRECHECK_DATE',
            'TMP_PARAM',
            'IS_INTEGRATED',
            'IS_CONFIRMED',
            'MANAGER_ID',
            'CONFIRM_SOURCE',
            'ZAL_ID',
            'IS_RESTOWEEK',
        ],
    ]) ?>

</div>
