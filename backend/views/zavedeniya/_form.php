<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\color\ColorInput;
use dosamigos\ckeditor\CKEditor;
use yii2mod\chosen\ChosenSelect;
/* @var $this yii\web\View */
/* @var $model backend\models\Zavedeniya */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="zavedeniya-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#main" data-toggle="tab" aria-expanded="false">Основное</a></li>
            <li class=""><a href="#fb" data-toggle="tab" aria-expanded="true">Фрейм</a></li>
            <li class=""><a href="#location" data-toggle="tab" aria-expanded="false">Расположение</a></li>
            <li class=""><a href="#working_time" data-toggle="tab" aria-expanded="false">Время работы</a></li>
            <li class=""><a href="#discounts" data-toggle="tab" aria-expanded="false">Cкидки</a></li>
            <li class=""><a href="#cabinet" data-toggle="tab" aria-expanded="false">Кабинет</a></li>
            <li class=""><a href="#banquets" data-toggle="tab" aria-expanded="false">Банкеты</a></li>
            <li class=""><a href="#photo" data-toggle="tab" aria-expanded="false">Фото</a></li>
            <li class=""><a href="#menu" data-toggle="tab" aria-expanded="false">Меню</a></li>
            <li class=""><a href="#restoweek" data-toggle="tab" aria-expanded="false">Restoweek</a></li>
            <li class=""><a href="#managers" data-toggle="tab" aria-expanded="false">Мененджеры</a></li>
            <li class=""><a href="#other" data-toggle="tab" aria-expanded="false">Прочее</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="main">

                <ul class="nav nav-tabs">
                    <?php foreach (Yii::$app->params['siteLanguages'] as $key => $language): ?>
                        <li class="<?= $language['class'] ?>"><a href="#<?= $key ?>" data-toggle="tab"
                                                                 aria-expanded="false"><?= $language['label'] ?></a>
                        </li>
                    <?php endforeach; ?>
                </ul>

                <div class="tab-content">
                    <br>
                    <?php foreach (Yii::$app->params['siteLanguages'] as $key => $language): ?>
                        <div class="tab-pane <?= $language['class'] ?>" id="<?= $key ?>">
                            <?= $form->field($model, "post[$key][name]")->textInput()->label('Название') ?>
                            <?= $form->field($model, "post[$key][short_description]")->textarea(['rows' => 2])->label('Краткое описание') ?>
                            <?= $form->field($model, "post[$key][description]")->widget(CKEditor::className(), [
                                'options' => ['rows' => 6],
                                'preset' => 'basic'
                            ])->label('Описание') ?>
                            <?= $form->field($model, "post[$key][work_time]")->textInput()->label('Время работы') ?>

                        </div>
                    <?php endforeach; ?>

                </div>

                <p>
                    <b>Ссылка для ФБ фрейма:</b>
                    <a href="https://fb.reston.com.ua/<?= $model->ID ?>">https://fb.reston.com.ua/<?= $model->SLUG ?></a>
                    /
                    <a href="https://fb.reston.com.ua/<?= $model->SLUG ?>">https://fb.reston.com.ua/<?= $model->SLUG ?></a>
                </p>

                <?= $form->field($model, 'MAX_PEOPLE_COUNT')->textInput()->label('Максимальное количество людей для бронирования') ?>

                <?= $form->field($model, 'HIDE_ORDER_COMMENT')->checkbox(); ?>

                <?= $form->field($model, 'SITE')->textInput() ?>

                <?= $form->field($model, 'TYPE')->dropDownList($model->getZTypeDropdown()) ?>

                <?= $form->field($model, 'VID')->dropDownList($model->getZZTypesDropdown()) ?>

                <?= $form->field($model, 'AVG_BILL')->textInput() ?>

                <?= $form->field($model, 'TELEPHONE')->textInput() ?>
            </div>
            <div class="tab-pane" id="fb">

                <?= $form->field($model, 'NAME_FRAME')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'ADRESS_FRAME')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'FRAME_LANG')->radioList(['ru', 'ua']) ?>

                <?= $form->field($model, 'FRAME_COLOR_ISSET')->checkbox() ?>

                <?= $form->field($model, 'FRAME_COLOR')->widget(ColorInput::classname(), [
                    'options' => ['placeholder' => 'Выберите цвет ...'],
                ]); ?>

                <?= $form->field($model, 'NOAVAILABLE_FRAME_TEXT')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'SPECIAL_FRAME_TEXT')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'HIDE_TITLE')->checkbox() ?>

                <?= $form->field($model, 'SMS_SEND')->checkbox() ?>

                <?= $form->field($model, 'FRAME_MAP_ON')->checkbox() ?>

                <?= $form->field($model, 'FRAME_TITLE_ORDER')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'ADR_IN_FRAME')->textInput(['maxlength' => true]) ?>

            </div>
            <div class="tab-pane" id="location">

                <div class="row">
                    <div class="col-sm-7">
                        <?= $form->field($model, 'CITY')->dropDownList($model->getCitiesDropdown()) ?>

                        <?= $form->field($model, 'ADDRESS')->textInput() ?>

                        <?php if ($model->CITY): ?>
                            <?= $form->field($model, 'RAION')->dropDownList($model->getRaionDropdown()) ?>
                            <?= $form->field($model, 'LINE')->dropDownList($model->getSubwayLineDropdown()) ?>
                            <?= $form->field($model, 'SUBWAY')->dropDownList($model->getSubwayDropdown()) ?>
                        <?php endif; ?>
                    </div>
                    <div class="col-sm-5">
                        <br>
                        <div class="well">
                            MAP
                        </div>
                    </div>
                </div>


            </div>
            <div class="tab-pane" id="working_time">

                <?php $weekDays = array(1 => 'Понедельник', 2 => 'Вторник', 3 => 'Среда', 4 => 'Четверг', 5 => 'Пятница', 6 => 'Суббота', 7 => 'Воскресенье'); ?>


            </div>
            <div class="tab-pane" id="discounts">

                <?= $form->field($model, 'DISCOUNTS_DATA')->textInput() ?>

                <?= $form->field($model, 'DISCOUNTS_TYPE')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'CONDITION')->textInput(['maxlength' => true]) ?>

            </div>
            <div class="tab-pane" id="cabinet">
                <?= $form->field($model, 'STATISTICS_ON')->checkbox() ?>

                <?= $form->field($model, 'BOOKING_CABINET_ON')->checkbox() ?>

                <?= $form->field($model, 'ECAMPAIGN_ON')->checkbox() ?>
            </div>

            <div class="tab-pane" id="banquets">
                <?= $form->field($model, 'BANQUET_ON')->checkbox() ?>

                <?= $form->field($model, 'BANQUET_DISCOUNT')->textInput() ?>

                <?= $form->field($model, 'BANQUET_PRIORITY')->textInput() ?>

                <?= $form->field($model, 'BANQUET_CHECK')->textInput() ?>

                <?= $form->field($model, 'B_TYPES')->widget(ChosenSelect::className(), [
                    'items' => $model->getBTypesDropdown(),
                    'options' => [
                        'multiple' => true
                    ]
                ]);  ?>


            </div>

            <div class="tab-pane" id="photo">

                <?= $form->field($model, 'LOGO')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'VIDEO_DATA')->textarea(['rows' => 6]) ?>
            </div>
            <div class="tab-pane" id="menu">
                <?= $form->field($model, 'MENU_FILE')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'MENU_FILE_URL')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'MENU_FILE_TYPE')->textInput(['maxlength' => true]) ?>

            </div>
            <div class="tab-pane" id="restoweek">


                <?= $form->field($model, 'IS_CATALOG')->textInput() ?>

                <?= $form->field($model, 'IS_RESTOWEEK')->textInput() ?>

                <?= $form->field($model, 'RESTOWEEK_PRICE')->textInput() ?>

                <?= $form->field($model, 'RESTOWEEK_TYPE')->textInput() ?>

            </div>
            <div class="tab-pane" id="managers">
            </div>

            <div class="tab-pane" id="other">

                <?= $form->field($model, 'ADRESANT_SMS')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'PERSONS_LIMITS')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'AVAILABLE')->textInput() ?>

                <?= $form->field($model, 'VISIBLE')->textInput() ?>

                <?= $form->field($model, 'CLOSED_FOREVER')->textInput() ?>

                <?= $form->field($model, 'FIXED_MAIN')->textInput() ?>

                <?= $form->field($model, 'FIXED_RAND')->textInput() ?>


                <?= $form->field($model, 'SMS_APPROVE_ON')->checkbox() ?>


                <?= $form->field($model, 'FOURSQUARE_ID')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'BOOKING_CARD_ON')->textInput() ?>






                <?= $form->field($model, 'SUCCESS_ORDER_TEXT')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'TIME_SMS_WAIT')->textInput() ?>

                <?= $form->field($model, 'GOOGLE_ANALYSTICS_KEY')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'IS_EDGING')->textInput() ?>

                <?= $form->field($model, 'TRANSPORT_TARGET')->textInput(['maxlength' => true]) ?>

            </div>


        </div>
        <!-- /.tab-content -->
    </div>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
