<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ZavedeniyaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="zavedeniya-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'SITE') ?>

    <?= $form->field($model, 'ADDITION_DATE') ?>

    <?= $form->field($model, 'SUBWAY') ?>

    <?= $form->field($model, 'RAION') ?>

    <?php // echo $form->field($model, 'MARKER_ID') ?>

    <?php // echo $form->field($model, 'TYPE') ?>

    <?php // echo $form->field($model, 'VID') ?>

    <?php // echo $form->field($model, 'VISIBLE') ?>

    <?php // echo $form->field($model, 'CITY') ?>

    <?php // echo $form->field($model, 'AVG_BILL') ?>

    <?php // echo $form->field($model, 'TELEPHONE') ?>

    <?php // echo $form->field($model, 'DUTY_START') ?>

    <?php // echo $form->field($model, 'DUTY_END') ?>

    <?php // echo $form->field($model, 'SLUG') ?>

    <?php // echo $form->field($model, 'VIEWS') ?>

    <?php // echo $form->field($model, 'LOGO') ?>

    <?php // echo $form->field($model, 'VIDEO_DATA') ?>

    <?php // echo $form->field($model, 'PERSONS_LIMITS') ?>

    <?php // echo $form->field($model, 'DISCOUNTS_DATA') ?>

    <?php // echo $form->field($model, 'DISCOUNTS_TYPE') ?>

    <?php // echo $form->field($model, 'FIXED_MAIN') ?>

    <?php // echo $form->field($model, 'FIXED_RAND') ?>

    <?php // echo $form->field($model, 'MAIN_IMAGE_ID') ?>

    <?php // echo $form->field($model, 'MENU_FILE') ?>

    <?php // echo $form->field($model, 'MENU_FILE_URL') ?>

    <?php // echo $form->field($model, 'MENU_FILE_TYPE') ?>

    <?php // echo $form->field($model, 'AVAILABLE') ?>

    <?php // echo $form->field($model, 'BANQUET_ON') ?>

    <?php // echo $form->field($model, 'STATISTICS_ON') ?>

    <?php // echo $form->field($model, 'ECAMPAIGN_ON') ?>

    <?php // echo $form->field($model, 'SMS_APPROVE_ON') ?>

    <?php // echo $form->field($model, 'BOOKING_CABINET_ON') ?>

    <?php // echo $form->field($model, 'FOURSQUARE_ID') ?>

    <?php // echo $form->field($model, 'POSITION') ?>

    <?php // echo $form->field($model, 'CONDITION') ?>

    <?php // echo $form->field($model, 'BOOKING_CARD_ON') ?>

    <?php // echo $form->field($model, 'MAX_PEOPLE_COUNT') ?>

    <?php // echo $form->field($model, 'HIDE_ORDER_COMMENT') ?>

    <?php // echo $form->field($model, 'NAME_FRAME') ?>

    <?php // echo $form->field($model, 'FRAME_LANG') ?>

    <?php // echo $form->field($model, 'HIDE_TITLE') ?>

    <?php // echo $form->field($model, 'ADRESS_FRAME') ?>

    <?php // echo $form->field($model, 'ADRESANT_SMS') ?>

    <?php // echo $form->field($model, 'FRAME_TITLE_ORDER') ?>

    <?php // echo $form->field($model, 'BANQUET_CHECK') ?>

    <?php // echo $form->field($model, 'FRAME_COLOR_ISSET') ?>

    <?php // echo $form->field($model, 'FRAME_COLOR') ?>

    <?php // echo $form->field($model, 'SMS_SEND') ?>

    <?php // echo $form->field($model, 'SUCCESS_ORDER_TEXT') ?>

    <?php // echo $form->field($model, 'TIME_SMS_WAIT') ?>

    <?php // echo $form->field($model, 'NOAVAILABLE_FRAME_TEXT') ?>

    <?php // echo $form->field($model, 'SPECIAL_FRAME_TEXT') ?>

    <?php // echo $form->field($model, 'BANQUET_PRIORITY') ?>

    <?php // echo $form->field($model, 'GOOGLE_ANALYSTICS_KEY') ?>

    <?php // echo $form->field($model, 'CATALOG_PRIORITY') ?>

    <?php // echo $form->field($model, 'TOP_CATALOG_DATE') ?>

    <?php // echo $form->field($model, 'TOP_ZAVEDENIYA_DATE') ?>

    <?php // echo $form->field($model, 'TOP_BANQUETS_DATE') ?>

    <?php // echo $form->field($model, 'TOP_WEEK_POSITION') ?>

    <?php // echo $form->field($model, 'TOP_WEEK_DATE') ?>

    <?php // echo $form->field($model, 'FRAME_MAP_ON') ?>

    <?php // echo $form->field($model, 'IS_EDGING') ?>

    <?php // echo $form->field($model, 'EDGING_DATE') ?>

    <?php // echo $form->field($model, 'ORDERED_KITCHEN_IDS') ?>

    <?php // echo $form->field($model, 'ADR_IN_FRAME') ?>

    <?php // echo $form->field($model, 'CLOSED_FOREVER') ?>

    <?php // echo $form->field($model, 'IS_CATALOG') ?>

    <?php // echo $form->field($model, 'IS_RESTOWEEK') ?>

    <?php // echo $form->field($model, 'RESTOWEEK_PRICE') ?>

    <?php // echo $form->field($model, 'RESTOWEEK_TYPE') ?>

    <?php // echo $form->field($model, 'TRANSPORT_TARGET') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
