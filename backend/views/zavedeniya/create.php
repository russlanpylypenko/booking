<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Zavedeniya */

$this->title = 'Create Zavedeniya';
$this->params['breadcrumbs'][] = ['label' => 'Zavedeniyas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zavedeniya-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
