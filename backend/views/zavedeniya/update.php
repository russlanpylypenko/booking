<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Zavedeniya */

$this->title = 'Редактирование заведения: ' . $model->lang->NAME;
$this->params['breadcrumbs'][] = ['label' => 'Заведения', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->lang->NAME;
$this->params['breadcrumbs'][] = 'редактирование';
?>
<div class="zavedeniya-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
