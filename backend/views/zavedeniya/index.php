<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ZavedeniyaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список заведений';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zavedeniya-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-info']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'NAME',
                'format' => 'raw',
                'value' => function ($zavedeniye) {
                    return Html::a($zavedeniye->lang->NAME, ['update', 'id' => $zavedeniye->ID]);
                },
            ],

            [
                'attribute' => 'Тип заведения',
                'contentOptions' => ['class' => 'text-center'],
                'format' => 'raw',
                'value' => function ($zavedeniye) {
                    return $zavedeniye->type->lang->NAME;
                },
            ],

            [
                'attribute' => 'Вид заведения',
                'format' => 'raw',
                'contentOptions' => ['class' => 'text-center'],
                'value' => function ($zavedeniye) {
                    return $zavedeniye->vid->lang->NAME;
                },
            ],

            [
                'attribute' => 'Город',
                'format' => 'raw',
                'contentOptions' => ['class' => 'text-center'],
                'value' => function ($zavedeniye) {
                    return $zavedeniye->cITY->lang->NAME;
                },
            ],

            [
                'attribute' => 'Метро',
                'headerOptions' => ['class' => 'text-center'],
                'format' => 'raw',
                'contentOptions' => ['class' => 'text-center'],
                'value' => function ($zavedeniye) {
                    return $zavedeniye->subway ? $zavedeniye->subway->lang->NAME : "";
                },
            ],

            [
                'attribute' => 'ADDRESS',
                'format' => 'raw',
                'contentOptions' => ['style' => 'width:300px; white-space: normal;'],
            ],

            [
                'attribute' => 'ADDITION_DATE',
                'contentOptions' => ['class' => 'text-center'],
            ],


            [
                'attribute' => 'Доступно для брони?',
                'format' => 'raw',
                'headerOptions' => ['class' => 'text-lowercase small text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'value' => function ($zavedeniye) {
                    return $zavedeniye->AVAILABLE ? "да" : "нет";
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Действия',
                'headerOptions' => ['width' => '80'],
                'template' => '{show} {hide} {update} {delete} ',
                'buttons' => [
                    'show' => function ($url, $zavedeniye, $key) {
                        if ($zavedeniye->VISIBLE === 1) return false;
                        return Html::a('<span class="glyphicon glyphicon-eye-open" title="Отображать"></span>', $url);
                    },
                    'hide' => function ($url, $zavedeniye, $key) {
                        if ($zavedeniye->VISIBLE === 0) return false;
                        return Html::a('<span class="glyphicon glyphicon-eye-close text-danger" title="Скрыть"></span>', $url);
                    },
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
