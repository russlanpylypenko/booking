<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'CREATED')->textInput() ?>

    <?= $form->field($model, 'MODIFIED')->textInput() ?>

    <?= $form->field($model, 'VISIBLE')->textInput() ?>

    <?= $form->field($model, 'COUNT_VIEW')->textInput() ?>

    <?= $form->field($model, 'PATH')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'TYPE')->textInput() ?>

    <?= $form->field($model, 'INCLUDE')->textInput() ?>

    <?= $form->field($model, 'SORT')->textInput() ?>

    <?= $form->field($model, 'LOGO')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'LOCKED')->textInput() ?>

    <?= $form->field($model, 'DATE_START')->textInput() ?>

    <?= $form->field($model, 'DATE_END')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
