<aside class="main-sidebar">

    <section class="sidebar">


        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Меню', 'options' => ['class' => 'header']],
                  //  ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],

                    ['label' => 'Страницы', 'icon' => 'html5', 'url' => ['/pages']],

                    ['label' => 'Новости', 'icon' => 'newspaper-o', 'url' => ['/']],

                    [
                        'label' => 'Обзоры',
                        'icon' => 'file-text-o',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Обзоры', 'icon' => 'file-text-o', 'url' => ['/'],],
                            ['label' => 'RestOn рекомендует', 'icon' => 'plus-square', 'url' => ['/'],],
                            ['label' => 'Планирование', 'icon' => 'check-square-o', 'url' => ['/'],],

                        ],
                    ],

                    ['label' => 'Предложения', 'icon' => 'percent', 'url' => ['/']],

                    ['label' => 'Тесты', 'icon' => 'file-code-o', 'url' => ['/']],

                    ['label' => 'Брони', 'icon' => 'book', 'url' => ['/orders']],


                    [
                        'label' => 'Заведения',
                        'icon' => 'cutlery',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Заведения', 'icon' => 'list', 'url' => ['/zavedeniya'],],
                            ['label' => 'Топ заведения', 'icon' => 'bar-chart', 'url' => ['/'],],
                            ['label' => 'Топ 20 заведений', 'icon' => 'bar-chart', 'url' => ['/'],],
                            ['label' => 'Топ 10 заведений недели', 'icon' => 'bar-chart', 'url' => ['/'],],
                            ['label' => 'Топ посещений', 'icon' => 'bar-chart', 'url' => ['/'],],
                            ['label' => 'Типы заведений', 'icon' => 'cutlery', 'url' => ['/'],],
                            ['label' => 'Выды заведений', 'icon' => 'cutlery', 'url' => ['/'],],
                            ['label' => 'Группы зведений', 'icon' => 'object-group', 'url' => ['/'],],
                            ['label' => '15 банктетных залов', 'icon' => 'bar-chart', 'url' => ['/'],],
                            ['label' => 'Типы банкетов', 'icon' => 'cutlery', 'url' => ['/'],],
                            ['label' => 'Опции', 'icon' => 'list-ol', 'url' => ['/'],],
                        ],
                    ],

                    [
                        'label' => 'Пользователи',
                        'icon' => 'cutlery',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Пользователи', 'icon' => 'list', 'url' => ['/'],],
                            ['label' => 'Группы пользователей', 'icon' => 'users', 'url' => ['/'],],

                        ],
                    ],

                    [
                        'label' => 'Система',
                        'icon' => 'gear',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Конфигурация системы', 'icon' => 'gear', 'url' => ['/'],],
                        ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
