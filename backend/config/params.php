<?php
return [
    'adminEmail' => 'admin@example.com',
    'siteLanguages' => [
        'ru' => ['label' => 'Русский', 'class' => 'active'],
        'uk' => ['label' => 'Українська', 'class' => ''],
      ]
];
