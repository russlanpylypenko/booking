<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "yii_lines".
 *
 * @property int $ID
 * @property int $CITY
 */
class Lines extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_lines';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CITY'], 'required'],
            [['CITY'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'CITY' => 'City',
        ];
    }

    public function getLang($lang = 'ru')
    {
        return $this->hasOne(LinesLang::className(), ['LINE_ID' => 'ID'])->where(['LANG' => $lang]);
    }
}
