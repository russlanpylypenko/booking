<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Orders;

/**
 * OrderSearch represents the model behind the search form of `backend\models\Orders`.
 */
class OrderSearch extends Orders
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID', 'USER_ID', 'ZAVEDENIYE_ID', 'TRANSACTION_ID', 'MAIL_SENT', 'SMS_SENT', 'PAID', 'ORDER_DISCOUNT', 'ORDER_DISCOUNT_TYPE', 'ORDER_PERSONS', 'REF_ID', 'IS_FB', 'IS_BANQUET', 'IS_DELETED', 'TABLE_ID', 'MAP_ID', 'HOSTES_RESERV', 'MESSAGE_SET', 'IS_CANCELED', 'IN_PROCESS', 'PREPAYMENT', 'IS_INTEGRATED', 'IS_CONFIRMED', 'MANAGER_ID', 'ZAL_ID', 'IS_RESTOWEEK'], 'integer'],
            [['CUSTOM', 'DATE', 'ORDER_COMMENT', 'ORDER_DATE', 'ORDER_TIME', 'ORDER_KEY', 'CANCEL_DATE', 'STATUS', 'REF_URL', 'REF_NAME', 'IP', 'CLIENT_CITY', 'CLIENT_STREET', 'CLIENT_REGION', 'CARD_NUMBER', 'TABLE_NUM', 'CAME_DATE', 'CUSTOMER_NAME', 'CLOSE_DATE', 'PRECHECK_DATE', 'TMP_PARAM', 'CONFIRM_SOURCE'], 'safe'],
            [['ORDER_AMT', 'CLIENT_LAT', 'CLIENT_LNG'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Orders::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'USER_ID' => $this->USER_ID,
            'ZAVEDENIYE_ID' => $this->ZAVEDENIYE_ID,
            'TRANSACTION_ID' => $this->TRANSACTION_ID,
            'MAIL_SENT' => $this->MAIL_SENT,
            'SMS_SENT' => $this->SMS_SENT,
            'PAID' => $this->PAID,
            'DATE' => $this->DATE,
            'ORDER_DISCOUNT' => $this->ORDER_DISCOUNT,
            'ORDER_DISCOUNT_TYPE' => $this->ORDER_DISCOUNT_TYPE,
            'ORDER_DATE' => $this->ORDER_DATE,
            'ORDER_TIME' => $this->ORDER_TIME,
            'ORDER_PERSONS' => $this->ORDER_PERSONS,
            'ORDER_AMT' => $this->ORDER_AMT,
            'CANCEL_DATE' => $this->CANCEL_DATE,
            'REF_ID' => $this->REF_ID,
            'CLIENT_LAT' => $this->CLIENT_LAT,
            'CLIENT_LNG' => $this->CLIENT_LNG,
            'IS_FB' => $this->IS_FB,
            'IS_BANQUET' => $this->IS_BANQUET,
            'IS_DELETED' => $this->IS_DELETED,
            'TABLE_ID' => $this->TABLE_ID,
            'MAP_ID' => $this->MAP_ID,
            'HOSTES_RESERV' => $this->HOSTES_RESERV,
            'MESSAGE_SET' => $this->MESSAGE_SET,
            'IS_CANCELED' => $this->IS_CANCELED,
            'IN_PROCESS' => $this->IN_PROCESS,
            'CAME_DATE' => $this->CAME_DATE,
            'PREPAYMENT' => $this->PREPAYMENT,
            'CLOSE_DATE' => $this->CLOSE_DATE,
            'PRECHECK_DATE' => $this->PRECHECK_DATE,
            'IS_INTEGRATED' => $this->IS_INTEGRATED,
            'IS_CONFIRMED' => $this->IS_CONFIRMED,
            'MANAGER_ID' => $this->MANAGER_ID,
            'ZAL_ID' => $this->ZAL_ID,
            'IS_RESTOWEEK' => $this->IS_RESTOWEEK,
        ]);

        $query->andFilterWhere(['like', 'CUSTOM', $this->CUSTOM])
            ->andFilterWhere(['like', 'ORDER_COMMENT', $this->ORDER_COMMENT])
            ->andFilterWhere(['like', 'ORDER_KEY', $this->ORDER_KEY])
            ->andFilterWhere(['like', 'STATUS', $this->STATUS])
            ->andFilterWhere(['like', 'REF_URL', $this->REF_URL])
            ->andFilterWhere(['like', 'REF_NAME', $this->REF_NAME])
            ->andFilterWhere(['like', 'IP', $this->IP])
            ->andFilterWhere(['like', 'CLIENT_CITY', $this->CLIENT_CITY])
            ->andFilterWhere(['like', 'CLIENT_STREET', $this->CLIENT_STREET])
            ->andFilterWhere(['like', 'CLIENT_REGION', $this->CLIENT_REGION])
            ->andFilterWhere(['like', 'CARD_NUMBER', $this->CARD_NUMBER])
            ->andFilterWhere(['like', 'TABLE_NUM', $this->TABLE_NUM])
            ->andFilterWhere(['like', 'CUSTOMER_NAME', $this->CUSTOMER_NAME])
            ->andFilterWhere(['like', 'TMP_PARAM', $this->TMP_PARAM])
            ->andFilterWhere(['like', 'CONFIRM_SOURCE', $this->CONFIRM_SOURCE]);

        return $dataProvider;
    }
}
