<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "yii_b_types_lang".
 *
 * @property int $ID
 * @property int $BTYPE_ID
 * @property string $NAME
 * @property string $NAME_R_P
 * @property string $SEO_TITLE
 * @property string $SEO_H1
 * @property string $SEO_DESC
 * @property string $SEO_TEXT
 * @property string $SEO_HEADING
 * @property string $LANG
 */
class BTypesLang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_b_types_lang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['BTYPE_ID'], 'required'],
            [['BTYPE_ID'], 'integer'],
            [['NAME', 'SEO_DESC', 'SEO_TEXT'], 'string'],
            [['NAME_R_P', 'SEO_TITLE', 'SEO_H1', 'SEO_HEADING'], 'string', 'max' => 255],
            [['LANG'], 'string', 'max' => 2],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'BTYPE_ID' => 'Btype ID',
            'NAME' => 'Name',
            'NAME_R_P' => 'Name R P',
            'SEO_TITLE' => 'Seo Title',
            'SEO_H1' => 'Seo H1',
            'SEO_DESC' => 'Seo Desc',
            'SEO_TEXT' => 'Seo Text',
            'SEO_HEADING' => 'Seo Heading',
            'LANG' => 'Lang',
        ];
    }
}
