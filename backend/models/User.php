<?php

namespace backend\models;

use app\models\Profile;
use Yii;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "yii_user".
 *
 * @property int $ID
 * @property string $LOGIN
 * @property string $PASSWORD
 * @property string $EMAIL
 * @property int $ROLE
 * @property string $IP
 * @property string $auth_key
 * @property int $DATE_REG
 * @property int $LAST_VISIT
 * @property int $CONFIRMED
 * @property int $SUBSCRIBE
 * @property int $REF
 * @property string $FB
 * @property string $VK
 * @property string $PHOTO
 * @property string $CONFIRM_KEY
 * @property int $ZAVEDENIE_ID
 * @property int $IS_HOSTESS
 * @property int $IS_DEMO
 * @property string $TELEGRAM_CHAT_ID
 * @property int $CHANGE_EMAIL
 * @property string $MANAGER_EMAIL
 * @property double $ACCOUNT_BALANCE
 *
 *
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_user';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'LOGIN' => 'Login',
            'PASSWORD' => 'Password',
            'EMAIL' => 'Email',
            'ROLE' => 'Role',
            'IP' => 'Ip',
            'DATE_REG' => 'Date Reg',
            'LAST_VISIT' => 'Last Visit',
            'CONFIRMED' => 'Confirmed',
            'SUBSCRIBE' => 'Subscribe',
            'REF' => 'Ref',
            'FB' => 'Fb',
            'VK' => 'Vk',
            'PHOTO' => 'Photo',
            'CONFIRM_KEY' => 'Confirm Key',
            'ZAVEDENIE_ID' => 'Zavedenie ID',
            'IS_HOSTESS' => 'Is Hostess',
            'IS_DEMO' => 'Is Demo',
            'TELEGRAM_CHAT_ID' => 'Telegram Chat ID',
            'CHANGE_EMAIL' => 'Change Email',
            'MANAGER_EMAIL' => 'Manager Email',
            'ACCOUNT_BALANCE' => 'Account Balance',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['ID' => $id]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::find()->where(['EMAIL' => $email])->orWhere(['LOGIN' => $email])->one();
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
           // 'password_reset_token' => $token
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->PASSWORD === $this->kmd5($password);
    }

    private function kmd5($password, $salt = null){
        if ($salt == null) {
            $salt = Yii::$app->params['key'];
        }
        return md5(md5($password) . sha1($salt));
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password, $salt = null)
    {
        $this->PASSWORD = $this->kmd5($password, $salt);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
       // $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        //$this->password_reset_token = null;
    }

    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['USER_ID' => 'ID']);
    }

}
