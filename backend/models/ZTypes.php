<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "yii_z_types".
 *
 * @property int $ID
 * @property string $SLUG
 * @property int $ORDER
 *
 */
class ZTypes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_z_types';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ORDER'], 'integer'],
            [['SLUG'], 'string', 'max' => 100],
            [['SLUG'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'SLUG' => 'Slug',
            'ORDER' => 'Order',
        ];
    }

    public function getLang()
    {
        return $this->hasOne(ZTypesLang::className(), ['ZTYPE_ID' => 'ID'])->where(['LANG' => 'ru']);
    }
}
