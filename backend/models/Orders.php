<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "yii_orders".
 *
 * @property int $ID
 * @property int $USER_ID
 * @property int $ZAVEDENIYE_ID
 * @property int $TRANSACTION_ID
 * @property int $MAIL_SENT
 * @property int $SMS_SENT
 * @property int $PAID
 * @property string $CUSTOM
 * @property string $DATE
 * @property string $ORDER_COMMENT
 * @property int $ORDER_DISCOUNT
 * @property int $ORDER_DISCOUNT_TYPE
 * @property string $ORDER_DATE
 * @property string $ORDER_TIME
 * @property int $ORDER_PERSONS
 * @property string $ORDER_KEY
 * @property double $ORDER_AMT
 * @property string $CANCEL_DATE
 * @property string $STATUS
 * @property string $REF_URL
 * @property string $REF_NAME
 * @property int $REF_ID
 * @property string $IP
 * @property double $CLIENT_LAT
 * @property double $CLIENT_LNG
 * @property string $CLIENT_CITY
 * @property string $CLIENT_STREET
 * @property string $CLIENT_REGION
 * @property int $IS_FB
 * @property int $IS_BANQUET
 * @property int $IS_DELETED
 * @property string $CARD_NUMBER
 * @property string $TABLE_NUM
 * @property int $TABLE_ID
 * @property int $MAP_ID
 * @property int $HOSTES_RESERV
 * @property int $MESSAGE_SET
 * @property int $IS_CANCELED
 * @property int $IN_PROCESS
 * @property string $CAME_DATE
 * @property string $CUSTOMER_NAME
 * @property int $PREPAYMENT
 * @property string $CLOSE_DATE
 * @property string $PRECHECK_DATE
 * @property string $TMP_PARAM
 * @property int $IS_INTEGRATED
 * @property int $IS_CONFIRMED
 * @property int $MANAGER_ID
 * @property string $CONFIRM_SOURCE
 * @property int $ZAL_ID
 * @property int $IS_RESTOWEEK
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['USER_ID', 'ZAVEDENIYE_ID', 'TRANSACTION_ID', 'MAIL_SENT', 'SMS_SENT', 'PAID', 'ORDER_DISCOUNT', 'ORDER_DISCOUNT_TYPE', 'ORDER_PERSONS', 'REF_ID', 'IS_FB', 'IS_BANQUET', 'IS_DELETED', 'TABLE_ID', 'MAP_ID', 'HOSTES_RESERV', 'MESSAGE_SET', 'IS_CANCELED', 'IN_PROCESS', 'PREPAYMENT', 'IS_INTEGRATED', 'IS_CONFIRMED', 'MANAGER_ID', 'ZAL_ID', 'IS_RESTOWEEK'], 'integer'],
            [['ZAVEDENIYE_ID', 'MAIL_SENT', 'SMS_SENT', 'DATE', 'ORDER_COMMENT', 'ORDER_DATE', 'ORDER_PERSONS', 'ORDER_AMT', 'STATUS', 'REF_URL', 'REF_NAME', 'IP', 'CLIENT_LAT', 'CLIENT_LNG'], 'required'],
            [['DATE', 'ORDER_DATE', 'ORDER_TIME', 'CANCEL_DATE', 'CAME_DATE', 'CLOSE_DATE', 'PRECHECK_DATE'], 'safe'],
            [['ORDER_COMMENT'], 'string'],
            [['ORDER_AMT', 'CLIENT_LAT', 'CLIENT_LNG'], 'number'],
            [['CUSTOM', 'REF_URL', 'REF_NAME', 'CLIENT_CITY', 'CLIENT_STREET', 'CLIENT_REGION'], 'string', 'max' => 255],
            [['ORDER_KEY'], 'string', 'max' => 15],
            [['STATUS', 'IP'], 'string', 'max' => 20],
            [['CARD_NUMBER'], 'string', 'max' => 50],
            [['TABLE_NUM'], 'string', 'max' => 11],
            [['CUSTOMER_NAME'], 'string', 'max' => 400],
            [['TMP_PARAM'], 'string', 'max' => 120],
            [['CONFIRM_SOURCE'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'USER_ID' => 'User ID',
            'ZAVEDENIYE_ID' => 'Zavedeniye ID',
            'TRANSACTION_ID' => 'Transaction ID',
            'MAIL_SENT' => 'Mail Sent',
            'SMS_SENT' => 'Sms Sent',
            'PAID' => 'Paid',
            'CUSTOM' => 'Custom',
            'DATE' => 'Date',
            'ORDER_COMMENT' => 'Order Comment',
            'ORDER_DISCOUNT' => 'Order Discount',
            'ORDER_DISCOUNT_TYPE' => 'Order Discount Type',
            'ORDER_DATE' => 'Order Date',
            'ORDER_TIME' => 'Order Time',
            'ORDER_PERSONS' => 'Order Persons',
            'ORDER_KEY' => 'Order Key',
            'ORDER_AMT' => 'Order Amt',
            'CANCEL_DATE' => 'Cancel Date',
            'STATUS' => 'Status',
            'REF_URL' => 'Ref Url',
            'REF_NAME' => 'Ref Name',
            'REF_ID' => 'Ref ID',
            'IP' => 'Ip',
            'CLIENT_LAT' => 'Client Lat',
            'CLIENT_LNG' => 'Client Lng',
            'CLIENT_CITY' => 'Client City',
            'CLIENT_STREET' => 'Client Street',
            'CLIENT_REGION' => 'Client Region',
            'IS_FB' => 'Is Fb',
            'IS_BANQUET' => 'Is Banquet',
            'IS_DELETED' => 'Is Deleted',
            'CARD_NUMBER' => 'Card Number',
            'TABLE_NUM' => 'Table Num',
            'TABLE_ID' => 'Table ID',
            'MAP_ID' => 'Map ID',
            'HOSTES_RESERV' => 'Hostes Reserv',
            'MESSAGE_SET' => 'Message Set',
            'IS_CANCELED' => 'Is Canceled',
            'IN_PROCESS' => 'In Process',
            'CAME_DATE' => 'Came Date',
            'CUSTOMER_NAME' => 'Customer Name',
            'PREPAYMENT' => 'Prepayment',
            'CLOSE_DATE' => 'Close Date',
            'PRECHECK_DATE' => 'Precheck Date',
            'TMP_PARAM' => 'Tmp Param',
            'IS_INTEGRATED' => 'Is Integrated',
            'IS_CONFIRMED' => 'Is Confirmed',
            'MANAGER_ID' => 'Manager ID',
            'CONFIRM_SOURCE' => 'Confirm Source',
            'ZAL_ID' => 'Zal ID',
            'IS_RESTOWEEK' => 'Is Restoweek',
        ];
    }
}
