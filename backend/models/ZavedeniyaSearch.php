<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Zavedeniya;

/**
 * ZavedeniyaSearch represents the model behind the search form of `backend\models\Zavedeniya`.
 */
class ZavedeniyaSearch extends Zavedeniya
{
    public $NAME;
    public $ADDRESS;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID', 'SUBWAY', 'RAION', 'MARKER_ID', 'TYPE', 'VID', 'VISIBLE', 'CITY', 'AVG_BILL', 'DUTY_START', 'DUTY_END', 'VIEWS', 'FIXED_MAIN', 'FIXED_RAND', 'MAIN_IMAGE_ID', 'AVAILABLE', 'BANQUET_ON', 'STATISTICS_ON', 'ECAMPAIGN_ON', 'SMS_APPROVE_ON', 'BOOKING_CABINET_ON', 'POSITION', 'BOOKING_CARD_ON', 'MAX_PEOPLE_COUNT', 'HIDE_ORDER_COMMENT', 'HIDE_TITLE', 'BANQUET_CHECK', 'FRAME_COLOR_ISSET', 'SMS_SEND', 'TIME_SMS_WAIT', 'BANQUET_PRIORITY', 'CATALOG_PRIORITY', 'TOP_WEEK_POSITION', 'FRAME_MAP_ON', 'IS_EDGING', 'CLOSED_FOREVER', 'IS_CATALOG', 'IS_RESTOWEEK', 'RESTOWEEK_PRICE', 'RESTOWEEK_TYPE'], 'integer'],
            [['SITE', 'ADDITION_DATE', 'TELEPHONE', 'SLUG', 'LOGO', 'VIDEO_DATA', 'PERSONS_LIMITS', 'DISCOUNTS_DATA', 'DISCOUNTS_TYPE', 'MENU_FILE', 'MENU_FILE_URL', 'MENU_FILE_TYPE', 'FOURSQUARE_ID', 'CONDITION', 'NAME_FRAME', 'FRAME_LANG', 'ADRESS_FRAME', 'ADRESANT_SMS', 'FRAME_TITLE_ORDER', 'FRAME_COLOR', 'SUCCESS_ORDER_TEXT', 'NOAVAILABLE_FRAME_TEXT', 'SPECIAL_FRAME_TEXT', 'GOOGLE_ANALYSTICS_KEY', 'TOP_CATALOG_DATE', 'TOP_ZAVEDENIYA_DATE', 'TOP_BANQUETS_DATE', 'TOP_WEEK_DATE', 'EDGING_DATE', 'ORDERED_KITCHEN_IDS', 'ADR_IN_FRAME', 'TRANSPORT_TARGET'], 'safe'],
            [['NAME', "ADDRESS"], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Zavedeniya::find()->orderBy(['ADDITION_DATE' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

//        if (!($this->load($params) && $this->validate())) {
//            $query->joinWith(['lang']);
//            return $dataProvider;
//        }


        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'ADDITION_DATE' => $this->ADDITION_DATE,
            'SUBWAY' => $this->SUBWAY,
            'RAION' => $this->RAION,
            'MARKER_ID' => $this->MARKER_ID,
            'TYPE' => $this->TYPE,
            'VID' => $this->VID,
            'VISIBLE' => $this->VISIBLE,
            'CITY' => $this->CITY,
            'AVG_BILL' => $this->AVG_BILL,
            'DUTY_START' => $this->DUTY_START,
            'DUTY_END' => $this->DUTY_END,
            'VIEWS' => $this->VIEWS,
            'FIXED_MAIN' => $this->FIXED_MAIN,
            'FIXED_RAND' => $this->FIXED_RAND,
            'MAIN_IMAGE_ID' => $this->MAIN_IMAGE_ID,
            'AVAILABLE' => $this->AVAILABLE,
            'BANQUET_ON' => $this->BANQUET_ON,
            'STATISTICS_ON' => $this->STATISTICS_ON,
            'ECAMPAIGN_ON' => $this->ECAMPAIGN_ON,
            'SMS_APPROVE_ON' => $this->SMS_APPROVE_ON,
            'BOOKING_CABINET_ON' => $this->BOOKING_CABINET_ON,
            'POSITION' => $this->POSITION,
            'BOOKING_CARD_ON' => $this->BOOKING_CARD_ON,
            'MAX_PEOPLE_COUNT' => $this->MAX_PEOPLE_COUNT,
            'HIDE_ORDER_COMMENT' => $this->HIDE_ORDER_COMMENT,
            'HIDE_TITLE' => $this->HIDE_TITLE,
            'BANQUET_CHECK' => $this->BANQUET_CHECK,
            'FRAME_COLOR_ISSET' => $this->FRAME_COLOR_ISSET,
            'SMS_SEND' => $this->SMS_SEND,
            'TIME_SMS_WAIT' => $this->TIME_SMS_WAIT,
            'BANQUET_PRIORITY' => $this->BANQUET_PRIORITY,
            'CATALOG_PRIORITY' => $this->CATALOG_PRIORITY,
            'TOP_CATALOG_DATE' => $this->TOP_CATALOG_DATE,
            'TOP_ZAVEDENIYA_DATE' => $this->TOP_ZAVEDENIYA_DATE,
            'TOP_BANQUETS_DATE' => $this->TOP_BANQUETS_DATE,
            'TOP_WEEK_POSITION' => $this->TOP_WEEK_POSITION,
            'TOP_WEEK_DATE' => $this->TOP_WEEK_DATE,
            'FRAME_MAP_ON' => $this->FRAME_MAP_ON,
            'IS_EDGING' => $this->IS_EDGING,
            'EDGING_DATE' => $this->EDGING_DATE,
            'CLOSED_FOREVER' => $this->CLOSED_FOREVER,
            'IS_CATALOG' => $this->IS_CATALOG,
            'IS_RESTOWEEK' => $this->IS_RESTOWEEK,
            'RESTOWEEK_PRICE' => $this->RESTOWEEK_PRICE,
            'RESTOWEEK_TYPE' => $this->RESTOWEEK_TYPE,
        ]);

        $query->andFilterWhere(['like', 'SITE', $this->SITE])
            ->andFilterWhere(['like', 'TELEPHONE', $this->TELEPHONE])
            ->andFilterWhere(['like', 'SLUG', $this->SLUG])
            ->andFilterWhere(['like', 'LOGO', $this->LOGO])
            ->andFilterWhere(['like', 'VIDEO_DATA', $this->VIDEO_DATA])
            ->andFilterWhere(['like', 'PERSONS_LIMITS', $this->PERSONS_LIMITS])
            ->andFilterWhere(['like', 'DISCOUNTS_DATA', $this->DISCOUNTS_DATA])
            ->andFilterWhere(['like', 'DISCOUNTS_TYPE', $this->DISCOUNTS_TYPE])
            ->andFilterWhere(['like', 'MENU_FILE', $this->MENU_FILE])
            ->andFilterWhere(['like', 'MENU_FILE_URL', $this->MENU_FILE_URL])
            ->andFilterWhere(['like', 'MENU_FILE_TYPE', $this->MENU_FILE_TYPE])
            ->andFilterWhere(['like', 'FOURSQUARE_ID', $this->FOURSQUARE_ID])
            ->andFilterWhere(['like', 'CONDITION', $this->CONDITION])
            ->andFilterWhere(['like', 'NAME_FRAME', $this->NAME_FRAME])
            ->andFilterWhere(['like', 'FRAME_LANG', $this->FRAME_LANG])
            ->andFilterWhere(['like', 'ADRESS_FRAME', $this->ADRESS_FRAME])
            ->andFilterWhere(['like', 'ADRESANT_SMS', $this->ADRESANT_SMS])
            ->andFilterWhere(['like', 'FRAME_TITLE_ORDER', $this->FRAME_TITLE_ORDER])
            ->andFilterWhere(['like', 'FRAME_COLOR', $this->FRAME_COLOR])
            ->andFilterWhere(['like', 'SUCCESS_ORDER_TEXT', $this->SUCCESS_ORDER_TEXT])
            ->andFilterWhere(['like', 'NOAVAILABLE_FRAME_TEXT', $this->NOAVAILABLE_FRAME_TEXT])
            ->andFilterWhere(['like', 'SPECIAL_FRAME_TEXT', $this->SPECIAL_FRAME_TEXT])
            ->andFilterWhere(['like', 'GOOGLE_ANALYSTICS_KEY', $this->GOOGLE_ANALYSTICS_KEY])
            ->andFilterWhere(['like', 'ORDERED_KITCHEN_IDS', $this->ORDERED_KITCHEN_IDS])
            ->andFilterWhere(['like', 'TRANSPORT_TARGET', $this->TRANSPORT_TARGET]);

        if ($this->NAME) {
            $query->joinWith(['lang' => function ($q) {
                $q->where('yii_zavedeniya_lang.NAME LIKE "%' . $this->NAME . '%"');
            }]);
        }

        if ($this->ADDRESS) {
            $query->joinWith(['lang' => function ($q) {
                $q->where('yii_zavedeniya_lang.ADDRESS LIKE "%' . $this->ADDRESS . '%"');
            }]);
        }
        return $dataProvider;
    }
}
