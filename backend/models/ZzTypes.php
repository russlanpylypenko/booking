<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "yii_zz_types".
 *
 * @property int $ID
 * @property string $SLUG
 * @property int $ORDER
 */
class ZzTypes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_zz_types';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'SLUG' => 'Slug',
            'ORDER' => 'Order',
        ];
    }


    public function getLang()
    {
        return $this->hasOne(ZzTypesLang::className(), ['ZZTYPE_ID' => 'ID'])->where(['LANG' => 'ru']);
    }
}
