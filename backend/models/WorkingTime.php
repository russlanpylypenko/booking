<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "yii_working_time".
 *
 * @property int $ID
 * @property int $ZAVEDENIYE_ID
 * @property int $ON
 * @property string $DATE
 * @property int $DAY_OF_WEEK
 * @property string $TIME_FROM
 * @property string $TIME_TO
 * @property string $TIME_FROM_1
 * @property string $TIME_TO_1
 */
class WorkingTime extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_working_time';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ZAVEDENIYE_ID', 'ON', 'DATE', 'DAY_OF_WEEK', 'TIME_FROM', 'TIME_TO', 'TIME_FROM_1', 'TIME_TO_1'], 'required'],
            [['ZAVEDENIYE_ID', 'ON', 'DAY_OF_WEEK'], 'integer'],
            [['DATE', 'TIME_FROM', 'TIME_TO', 'TIME_FROM_1', 'TIME_TO_1'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ZAVEDENIYE_ID' => 'Zavedeniye ID',
            'ON' => 'On',
            'DATE' => 'Date',
            'DAY_OF_WEEK' => 'Day Of Week',
            'TIME_FROM' => 'Time From',
            'TIME_TO' => 'Time To',
            'TIME_FROM_1' => 'Time From 1',
            'TIME_TO_1' => 'Time To 1',
        ];
    }
}
