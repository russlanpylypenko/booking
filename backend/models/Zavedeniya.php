<?php

namespace backend\models;

use app\models\BTypes;
use app\models\BTypesRelations;
use app\models\WorkingTime;
use kartik\base\Widget;
use SebastianBergmann\Diff\Line;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "yii_zavedeniya".
 *
 * @property int $ID
 * @property string $SITE
 * @property string $ADDITION_DATE
 * @property int $SUBWAY
 * @property int $RAION
 * @property int $MARKER_ID
 * @property int $TYPE
 * @property int $VID
 * @property int $VISIBLE
 * @property int $CITY
 * @property int $AVG_BILL
 * @property string $TELEPHONE
 * @property int $DUTY_START
 * @property int $DUTY_END
 * @property string $SLUG
 * @property int $VIEWS
 * @property string $LOGO
 * @property string $VIDEO_DATA
 * @property string $PERSONS_LIMITS
 * @property string $DISCOUNTS_DATA
 * @property string $DISCOUNTS_TYPE
 * @property int $FIXED_MAIN
 * @property int $FIXED_RAND
 * @property int $MAIN_IMAGE_ID
 * @property string $MENU_FILE
 * @property string $MENU_FILE_URL
 * @property string $MENU_FILE_TYPE
 * @property int $AVAILABLE
 * @property int $BANQUET_ON
 * @property int $STATISTICS_ON
 * @property int $ECAMPAIGN_ON
 * @property int $SMS_APPROVE_ON
 * @property int $BOOKING_CABINET_ON
 * @property string $FOURSQUARE_ID
 * @property int $POSITION
 * @property string $CONDITION
 * @property int $BOOKING_CARD_ON
 * @property int $MAX_PEOPLE_COUNT
 * @property int $HIDE_ORDER_COMMENT
 * @property string $NAME_FRAME
 * @property string $FRAME_LANG
 * @property int $HIDE_TITLE
 * @property string $ADRESS_FRAME
 * @property string $ADRESANT_SMS
 * @property string $FRAME_TITLE_ORDER
 * @property int $BANQUET_CHECK
 * @property int $FRAME_COLOR_ISSET
 * @property string $FRAME_COLOR
 * @property int $SMS_SEND
 * @property string $SUCCESS_ORDER_TEXT
 * @property int $TIME_SMS_WAIT
 * @property string $NOAVAILABLE_FRAME_TEXT
 * @property string $SPECIAL_FRAME_TEXT
 * @property int $BANQUET_PRIORITY
 * @property string $GOOGLE_ANALYSTICS_KEY
 * @property int $CATALOG_PRIORITY
 * @property string $TOP_CATALOG_DATE
 * @property string $TOP_ZAVEDENIYA_DATE
 * @property string $TOP_BANQUETS_DATE
 * @property int $TOP_WEEK_POSITION
 * @property string $TOP_WEEK_DATE
 * @property int $FRAME_MAP_ON
 * @property int $IS_EDGING
 * @property string $EDGING_DATE
 * @property string $ORDERED_KITCHEN_IDS
 * @property string $ADR_IN_FRAME
 * @property int $CLOSED_FOREVER
 * @property int $IS_CATALOG
 * @property int $IS_RESTOWEEK
 * @property int $RESTOWEEK_PRICE
 * @property int $RESTOWEEK_TYPE
 * @property string $TRANSPORT_TARGET
 *
 * @property YiiKitchenRelations[] $yiiKitchenRelations
 * @property YiiOptionsRelations[] $yiiOptionsRelations
 * @property YiiSpecialDiscount[] $yiiSpecialDiscounts
 * @property YiiUserNote[] $yiiUserNotes
 * @property Cities $cITY
 * @property YiiZTypes $tYPE
 * @property YiiZavedeniyaUsersRelations[] $yiiZavedeniyaUsersRelations
 */
class Zavedeniya extends \yii\db\ActiveRecord
{
    public $LINE;
    public $post;
    public $working_time;
    public $BANQUET_DISCOUNT;
    public $B_TYPES;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_zavedeniya';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ADDITION_DATE', 'SUBWAY', 'RAION', 'MARKER_ID', 'TELEPHONE', 'SLUG', 'LOGO', 'VIDEO_DATA', 'DISCOUNTS_DATA', 'DISCOUNTS_TYPE', 'MAIN_IMAGE_ID', 'MENU_FILE', 'MENU_FILE_URL', 'MENU_FILE_TYPE', 'BANQUET_ON', 'BOOKING_CABINET_ON', 'NAME_FRAME', 'ADRESS_FRAME', 'ADRESANT_SMS', 'FRAME_TITLE_ORDER', 'BANQUET_CHECK', 'FRAME_COLOR', 'SUCCESS_ORDER_TEXT', 'TIME_SMS_WAIT', 'NOAVAILABLE_FRAME_TEXT', 'SPECIAL_FRAME_TEXT', 'GOOGLE_ANALYSTICS_KEY', 'TOP_CATALOG_DATE', 'TOP_ZAVEDENIYA_DATE', 'TOP_BANQUETS_DATE', 'TOP_WEEK_DATE', 'ORDERED_KITCHEN_IDS', 'ADR_IN_FRAME', 'TRANSPORT_TARGET'], 'required'],
            [['SITE', 'TELEPHONE', 'VIDEO_DATA', 'PERSONS_LIMITS', 'DISCOUNTS_DATA', 'ORDERED_KITCHEN_IDS'], 'string'],
            [['ADDITION_DATE', 'TOP_CATALOG_DATE', 'TOP_ZAVEDENIYA_DATE', 'TOP_BANQUETS_DATE', 'TOP_WEEK_DATE', 'EDGING_DATE'], 'safe'],
            [['SUBWAY', 'RAION', 'MARKER_ID', 'TYPE', 'VID', 'VISIBLE', 'CITY', 'AVG_BILL', 'DUTY_START', 'DUTY_END', 'VIEWS', 'FIXED_MAIN', 'FIXED_RAND', 'MAIN_IMAGE_ID', 'AVAILABLE', 'BANQUET_ON', 'STATISTICS_ON', 'ECAMPAIGN_ON', 'SMS_APPROVE_ON', 'BOOKING_CABINET_ON', 'POSITION', 'BOOKING_CARD_ON', 'MAX_PEOPLE_COUNT', 'HIDE_ORDER_COMMENT', 'HIDE_TITLE', 'BANQUET_CHECK', 'FRAME_COLOR_ISSET', 'SMS_SEND', 'TIME_SMS_WAIT', 'BANQUET_PRIORITY', 'CATALOG_PRIORITY', 'TOP_WEEK_POSITION', 'FRAME_MAP_ON', 'IS_EDGING', 'CLOSED_FOREVER', 'IS_CATALOG', 'IS_RESTOWEEK', 'RESTOWEEK_PRICE', 'RESTOWEEK_TYPE'], 'integer'],
            [['SLUG', 'MENU_FILE_URL', 'FOURSQUARE_ID', 'CONDITION'], 'string', 'max' => 255],
            [['LOGO', 'MENU_FILE', 'MENU_FILE_TYPE'], 'string', 'max' => 50],
            [['DISCOUNTS_TYPE'], 'string', 'max' => 15],
            [['NAME_FRAME', 'NOAVAILABLE_FRAME_TEXT', 'SPECIAL_FRAME_TEXT'], 'string', 'max' => 300],
            [['FRAME_LANG'], 'string', 'max' => 2],
            [['ADRESS_FRAME', 'ADRESANT_SMS'], 'string', 'max' => 100],
            [['FRAME_TITLE_ORDER'], 'string', 'max' => 500],
            [['FRAME_COLOR'], 'string', 'max' => 11],
            [['SUCCESS_ORDER_TEXT'], 'string', 'max' => 1000],
            [['GOOGLE_ANALYSTICS_KEY'], 'string', 'max' => 30],
            [['ADR_IN_FRAME'], 'string', 'max' => 10],
            [['TRANSPORT_TARGET'], 'string', 'max' => 20],
            [['CITY'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['CITY' => 'ID']],
            [['TYPE'], 'exist', 'skipOnError' => true, 'targetClass' => ZTypes::className(), 'targetAttribute' => ['TYPE' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'SITE' => 'Сайт',
            'ADDITION_DATE' => 'Дата добавления',
            'SUBWAY' => 'Subway',
            'RAION' => 'Raion',
            'MARKER_ID' => 'Marker ID',
            'TYPE' => 'Тип зведения',
            'VID' => 'Вид заведения',
            'VISIBLE' => 'Visible',
            'CITY' => 'Город',
            'AVG_BILL' => 'Средний чек',
            'TELEPHONE' => 'Телефон',
            'DUTY_START' => 'Duty Start',
            'DUTY_END' => 'Duty End',
            'DUTY' => 'Режим работы',
            'SLUG' => 'Slug',
            'VIEWS' => 'Views',
            'LOGO' => 'Logo',
            'VIDEO_DATA' => 'Video Data',
            'PERSONS_LIMITS' => 'Лимит человек',
            'DISCOUNTS_DATA' => 'Скидки',
            'DISCOUNTS_TYPE' => 'Тип скидок',
            'FIXED_MAIN' => 'Fixed Main',
            'FIXED_RAND' => 'Fixed Rand',
            'MAIN_IMAGE_ID' => 'Main Image ID',
            'MENU_FILE' => 'Menu File',
            'MENU_FILE_URL' => 'Menu File Url',
            'MENU_FILE_TYPE' => 'Menu File Type',
            'AVAILABLE' => 'Доступно для брони?',
            'BANQUET_ON' => 'Банкеты включены?',
            'STATISTICS_ON' => 'Статистика включена',
            'ECAMPAIGN_ON' => 'Рассылка включена',
            'SMS_APPROVE_ON' => 'Sms Approve On',
            'BOOKING_CABINET_ON' => 'Кабинет администратора включен',
            'FOURSQUARE_ID' => 'Foursquare ID',
            'POSITION' => 'Position',
            'CONDITION' => 'Условие',
            'BOOKING_CARD_ON' => 'Booking Card On',
            'MAX_PEOPLE_COUNT' => 'Max People Count',
            'HIDE_ORDER_COMMENT' => 'Показывать пожелание к брони',
            'NAME_FRAME' => 'Название в фрейме',
            'FRAME_LANG' => 'Язык в фрейме',
            'HIDE_TITLE' => 'Не показивать название в фрейме',
            'ADRESS_FRAME' => 'Адрес в фрейме',
            'ADRESANT_SMS' => 'Адресант смс',
            'FRAME_TITLE_ORDER' => 'Frame Title Order',
            'BANQUET_CHECK' => 'Средний чек за банкет',
            'FRAME_COLOR_ISSET' => 'Цвет в фрейме?',
            'FRAME_COLOR' => 'Цвет в фрейме',
            'SMS_SEND' => 'Отправка СМС с фрейма',
            'SUCCESS_ORDER_TEXT' => 'Success Order Text',
            'TIME_SMS_WAIT' => 'Time Sms Wait',
            'NOAVAILABLE_FRAME_TEXT' => 'Текст для недоступного заведения в фрейме',
            'SPECIAL_FRAME_TEXT' => 'Дополнительный текст в фрейме',
            'BANQUET_PRIORITY' => 'Позиция банкета(сортировка по парам.)',
            'GOOGLE_ANALYSTICS_KEY' => 'Идентификатор отслеживания Google Analystic',
            'CATALOG_PRIORITY' => 'Catalog Priority',
            'TOP_CATALOG_DATE' => 'Top Catalog Date',
            'TOP_ZAVEDENIYA_DATE' => 'Top Zavedeniya Date',
            'TOP_BANQUETS_DATE' => 'Top Banquets Date',
            'TOP_WEEK_POSITION' => 'Top Week Position',
            'TOP_WEEK_DATE' => 'Top Week Date',
            'FRAME_MAP_ON' => 'План заведения в форме бронирования',
            'IS_EDGING' => 'Is Edging',
            'EDGING_DATE' => 'Edging Date',
            'ORDERED_KITCHEN_IDS' => 'Ordered Kitchen Ids',
            'ADR_IN_FRAME' => 'Adr In Frame',
            'CLOSED_FOREVER' => 'Closed Forever',
            'IS_CATALOG' => 'Is Catalog',
            'IS_RESTOWEEK' => 'Is Restoweek',
            'RESTOWEEK_PRICE' => 'Restoweek Price',
            'RESTOWEEK_TYPE' => 'Restoweek Type',
            'TRANSPORT_TARGET' => 'Сервер транспорта броней',
            'NAME' => 'Название заведения',
            'SHORT_DESCRIPTION' => 'Краткое описание',
            'ADDRESS' => 'Адрес',
            'LINE' => 'Ветка',
            'BANQUET_DISCOUNT' => 'Скидка на банкет',
            'B_TYPES' => 'Типы банкетов',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYiiKitchenRelations()
    {
        return $this->hasMany(YiiKitchenRelations::className(), ['FROM_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYiiOptionsRelations()
    {
        return $this->hasMany(YiiOptionsRelations::className(), ['FROM_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYiiSpecialDiscounts()
    {
        return $this->hasMany(YiiSpecialDiscount::className(), ['ZAVEDENIYE_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYiiUserNotes()
    {
        return $this->hasMany(YiiUserNote::className(), ['ZAVEDENIE_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCITY()
    {
        return $this->hasOne(Cities::className(), ['ID' => 'CITY']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(ZTypes::className(), ['ID' => 'TYPE']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVid()
    {
        return $this->hasOne(ZzTypes::className(), ['ID' => 'VID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubway()
    {
        return $this->hasOne(Subway::className(), ['ID' => 'SUBWAY']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYiiZavedeniyaUsersRelations()
    {
        return $this->hasMany(YiiZavedeniyaUsersRelations::className(), ['TO_ID' => 'ID']);
    }


    public function getLang($lang = 'ru')
    {
        return $this->hasOne(ZavedeniyaLang::className(), ['ZAVEDENIYE_ID' => 'ID'])->where(['LANG' => $lang]);
    }

    public function getWorkingTime()
    {
        return $this->hasMany(WorkingTime::className(),['ZAVEDENIYE_ID' => 'ID'])->where(['ON' => 1])->orderBy(['DAY_OF_WEEK' => SORT_ASC]);
    }

    public function getBTypeIds()
    {
        return $this->hasMany(BTypesRelations::className(),['FROM_ID' => 'ID'])->select('TO_ID')->asArray()->all();
    }

    public function getName()
    {
        return $this->lang->NAME;
    }

    public function getShort_Description(){
        return $this->lang->SHORT_DESCRIPTION;
    }

    public function getAddress()
    {
        return $this->lang->ADDRESS;
    }


    public function getCitiesDropdown()
    {
        $Cities = CitiesLang::find()->where(['LANG' => 'ru'])->asArray()->all();
        return ArrayHelper::map($Cities, 'CITY_ID', 'NAME');
    }

    public function getRaionDropdown()
    {
        $Raions = Raion::find()
            ->with('lang')
            //->where(['CITY' => $this->CITY])
            ->asArray()
            ->all();

        $dropdownList = ArrayHelper::map($Raions, 'ID', 'lang.NAME');

        $dropdownList = array_filter($dropdownList, function($element) {
            return !empty($element);
        });

        return $dropdownList;
    }

    public function getSubwayLineDropdown()
    {
        $Lines = Lines::find()
            ->with('lang')
            //->where(['CITY' => $this->CITY])
            ->asArray()
            ->all();

        $dropdownList = ArrayHelper::map($Lines, 'ID', 'lang.NAME');

        $dropdownList = array_filter($dropdownList, function($element) {
            return !empty($element);
        });

        return $dropdownList;
    }


    public function getSubwayDropdown()
    {
        $Subways = Subway::find()
            ->with('lang')
            //->where(['CITY' => $this->CITY])
            ->asArray()
            ->all();

        $dropdownList = ArrayHelper::map($Subways, 'ID', 'lang.NAME');

        $dropdownList = array_filter($dropdownList, function($element) {
            return !empty($element);
        });

        return $dropdownList;
    }

    public function getZTypeDropdown()
    {
        $ZTypes = ZTypes::find()
            ->with('lang')
            ->asArray()
            ->all();

        $dropdownList = ArrayHelper::map($ZTypes, 'ID', 'lang.NAME');

        $dropdownList = array_filter($dropdownList, function($element) {
            return !empty($element);
        });

        return $dropdownList;
    }

    public function getZZTypesDropdown()
    {
        $ZZTypes = ZzTypes::find()
            ->with('lang')
            ->asArray()
            ->all();

        $dropdownList = ArrayHelper::map($ZZTypes, 'ID', 'lang.NAME');

        $dropdownList = array_filter($dropdownList, function($element) {
            return !empty($element);
        });

        return $dropdownList;
    }

    public function getBTypesDropdown()
    {
        $BTypes = BTypes::find()
            ->with('lang')
            ->asArray()
            ->all();

        $dropdownList = ArrayHelper::map($BTypes, 'ID', 'lang.NAME');

        $dropdownList = array_filter($dropdownList, function($element) {
            return !empty($element);
        });

        return $dropdownList;
    }


    public function afterFind()
    {
        $this->LINE = $this->subway->LINE;

        $this->getPost();

        if($this->workingTime) {
            $this->working_time = $this->workingTime;
        }

        $this->B_TYPES =ArrayHelper::getColumn($this->getBTypeIds(),'TO_ID') ;


        parent::afterFind(); // TODO: Change the autogenerated stub
    }

    private function getPost()
    {
        foreach (Yii::$app->params['siteLanguages'] as $key => $language){
            $itemLang = $this->getLang($key)->one();
            $this->post[$key] = [
                'short_description' => $itemLang->SHORT_DESCRIPTION,
                'description' => html_entity_decode($itemLang->DESCRIPTION),
                'name' => $itemLang->NAME,
                'work_time' => $itemLang->WORK_TIME,
            ];
        }

    }

}
