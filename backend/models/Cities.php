<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "yii_cities".
 *
 * @property int $ID
 * @property string $SLUG
 *
 * @property YiiZavedeniya[] $yiiZavedeniyas
 */
class Cities extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_cities';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SLUG'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'SLUG' => 'Slug',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYiiZavedeniyas()
    {
        return $this->hasMany(YiiZavedeniya::className(), ['CITY' => 'ID']);
    }


    public function getLang()
    {
        return $this->hasOne(CitiesLang::className(), ['CITY_ID' => 'ID'])->where(['LANG' => 'ru']);
    }

}
