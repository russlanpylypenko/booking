<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "yii_page".
 *
 * @property int $ID
 * @property int $CREATED
 * @property int $MODIFIED
 * @property int $VISIBLE
 * @property int $COUNT_VIEW
 * @property string $PATH
 * @property int $TYPE 1-static,2-news,3-article,4-action,5-unews
 * @property int $INCLUDE
 * @property int $SORT
 * @property string $LOGO
 * @property int $LOCKED
 * @property string $DATE_START
 * @property string $DATE_END
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_page';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CREATED', 'MODIFIED', 'VISIBLE', 'COUNT_VIEW', 'PATH', 'SORT', 'LOGO', 'DATE_START', 'DATE_END'], 'required'],
            [['CREATED', 'MODIFIED', 'VISIBLE', 'COUNT_VIEW', 'TYPE', 'INCLUDE', 'SORT', 'LOCKED'], 'integer'],
            [['DATE_START', 'DATE_END'], 'safe'],
            [['PATH'], 'string', 'max' => 100],
            [['LOGO'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'CREATED' => 'Created',
            'MODIFIED' => 'Modified',
            'VISIBLE' => 'Visible',
            'COUNT_VIEW' => 'Count View',
            'PATH' => 'Path',
            'TYPE' => 'Type',
            'INCLUDE' => 'Include',
            'SORT' => 'Sort',
            'LOGO' => 'Logo',
            'LOCKED' => 'Locked',
            'DATE_START' => 'Date Start',
            'DATE_END' => 'Date End',
        ];
    }
}
