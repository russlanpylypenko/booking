<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "yii_z_types_lang".
 *
 * @property int $ID
 * @property int $ZTYPE_ID
 * @property string $NAME
 * @property string $LANG
 */
class ZTypesLang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_z_types_lang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ZTYPE_ID', 'NAME'], 'required'],
            [['ZTYPE_ID'], 'integer'],
            [['NAME'], 'string', 'max' => 255],
            [['LANG'], 'string', 'max' => 2],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ZTYPE_ID' => 'Ztype ID',
            'NAME' => 'Name',
            'LANG' => 'Lang',
        ];
    }
}
