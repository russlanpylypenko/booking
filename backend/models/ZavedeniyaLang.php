<?php

namespace backend\models;

use app\models\BTypesLang;
use Yii;

/**
 * This is the model class for table "yii_zavedeniya_lang".
 *
 * @property int $ID
 * @property int $ZAVEDENIYE_ID
 * @property string $NAME
 * @property string $ADDRESS
 * @property string $ADDRESS_FRAME
 * @property string $FRAME_SUCCESSFULLY_ORDER
 * @property string $DESCRIPTION
 * @property string $SHORT_DESCRIPTION
 * @property string $CONDITION
 * @property string $WORK_TIME
 * @property string $RESTOWEEK_DESCRIPTION
 * @property string $LANG
 */
class ZavedeniyaLang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_zavedeniya_lang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ZAVEDENIYE_ID', 'ADDRESS_FRAME', 'WORK_TIME'], 'required'],
            [['ZAVEDENIYE_ID'], 'integer'],
            [['NAME', 'ADDRESS', 'DESCRIPTION', 'SHORT_DESCRIPTION', 'CONDITION'], 'string'],
            [['ADDRESS_FRAME'], 'string', 'max' => 100],
            [['FRAME_SUCCESSFULLY_ORDER'], 'string', 'max' => 200],
            [['WORK_TIME', 'RESTOWEEK_DESCRIPTION'], 'string', 'max' => 255],
            [['LANG'], 'string', 'max' => 2],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ZAVEDENIYE_ID' => 'Zavedeniye ID',
            'NAME' => 'Name',
            'ADDRESS' => 'Address',
            'ADDRESS_FRAME' => 'Address Frame',
            'FRAME_SUCCESSFULLY_ORDER' => 'Frame Successfully Order',
            'DESCRIPTION' => 'Description',
            'SHORT_DESCRIPTION' => 'Short Description',
            'CONDITION' => 'Condition',
            'WORK_TIME' => 'Время работы',
            'RESTOWEEK_DESCRIPTION' => 'Restoweek Description',
            'LANG' => 'Lang',
        ];
    }

}
