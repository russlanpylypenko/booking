<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "yii_b_types_relations".
 *
 * @property int $TO_ID
 * @property int $FROM_ID
 */
class BTypesRelations extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_b_types_relations';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['TO_ID', 'FROM_ID'], 'required'],
            [['TO_ID', 'FROM_ID'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'TO_ID' => 'To ID',
            'FROM_ID' => 'From ID',
        ];
    }
}
