<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "yii_raion".
 *
 * @property int $ID
 * @property int $LINE
 * @property int $CITY
 * @property string $SLUG
 */
class Raion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_raion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['LINE', 'CITY'], 'integer'],
            [['CITY', 'SLUG'], 'required'],
            [['SLUG'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'LINE' => 'Line',
            'CITY' => 'City',
            'SLUG' => 'Slug',
        ];
    }

    public function getLang($lang = 'ru')
    {
        return $this->hasOne(RaionLang::className(), ['RAION_ID' => 'ID'])->where(['LANG' => $lang]);
    }
}
