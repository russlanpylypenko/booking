<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Page;

/**
 * PageSearch represents the model behind the search form of `backend\models\Page`.
 */
class PageSearch extends Page
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID', 'CREATED', 'MODIFIED', 'VISIBLE', 'COUNT_VIEW', 'TYPE', 'INCLUDE', 'SORT', 'LOCKED'], 'integer'],
            [['PATH', 'LOGO', 'DATE_START', 'DATE_END'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Page::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'CREATED' => $this->CREATED,
            'MODIFIED' => $this->MODIFIED,
            'VISIBLE' => $this->VISIBLE,
            'COUNT_VIEW' => $this->COUNT_VIEW,
            'TYPE' => $this->TYPE,
            'INCLUDE' => $this->INCLUDE,
            'SORT' => $this->SORT,
            'LOCKED' => $this->LOCKED,
            'DATE_START' => $this->DATE_START,
            'DATE_END' => $this->DATE_END,
        ]);

        $query->andFilterWhere(['like', 'PATH', $this->PATH])
            ->andFilterWhere(['like', 'LOGO', $this->LOGO]);

        return $dataProvider;
    }
}
