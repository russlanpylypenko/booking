<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "yii_b_types".
 *
 * @property int $ID
 * @property string $SLUG
 */
class BTypes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_b_types';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SLUG'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'SLUG' => 'Slug',
        ];
    }

    public function getLang($lang = 'ru')
    {
        return $this->hasOne(BTypesLang::className(), ['BTYPE_ID' => 'ID'])->where(['LANG' => $lang]);
    }
}
