<?php

namespace frontend\components;

use app\models\Cities;
use yii\helpers\Url;
use Yii;


class CityDropdown
{
    public $items;
//    private static $_labels;

    public function getLanguageItems(){
        $currentCityId = Yii::$app->params['city'];
        $cities = Cities::find()->with('lang')->where('ID != '. $currentCityId)->asArray()->all();
        $result = [];

        foreach ($cities as $city){
            $result[] = [
                'label' => $city['lang']["NAME"],
                'url' => Url::to(['set-city', 'id' => $city['ID']])
            ];
        }

       return $result;
    }

    public function getCurrentCity()
    {
        $cityId = Yii::$app->params['city'];
        return Cities::find()->where(['ID' => $cityId])->one();
    }
}