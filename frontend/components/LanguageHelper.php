<?php

namespace frontend\components;


class LanguageHelper
{
    public static function getLangCode()
    {
        $appLang = \Yii::$app->language;


        if ($appLang === 'ru') {
            return 'ru';
        }
        if($appLang === 'uk'){
            return 'uk';
        }

        return 'ru';
    }

}