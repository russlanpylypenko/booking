<?php

namespace frontend\components;
use yii\base\BootstrapInterface;


class CityBootstrap implements BootstrapInterface
{

    const DEFAULT_CITY_ID = 1;

    public function bootstrap($app)
    {
        $preferredCity = isset($app->request->cookies['city']) ? (string)$app->request->cookies['city'] : self::DEFAULT_CITY_ID;

        $app->params['city'] = $preferredCity;
    }
}