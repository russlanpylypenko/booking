var restaurantId;
var csrfToken = $('meta[name=csrf-token]').attr("content");

$('.order-btn').on('click', function (e) {
    e.preventDefault();
    var btn = $(this);

    restaurantId = btn.data('id');

    $("#orderModal").modal('show');

});

$("#orderModal").on('show.bs.modal', function(){
   $.ajax({
       url: '/order/get-available-times',
       method: 'POST',
       data: {restaurant: restaurantId},
       success: function (response) {
           $('.time-wrapper').empty();
            response.times.map(function (item) {
                $('.time-wrapper').append('<button type="button" data-time="'+ item +'" class="btn btn-xs order-time-btn">'+ item +'</button> ')
            });
            
            bindSelectTime()
       }
   });

    bindSendForm();
});


function bindSelectTime() {
    $('.order-time-btn').unbind();
    
    $('.order-time-btn').on('click',function () {
        $('.order-time-btn').removeClass('btn-success');
        $('#order-time-input').val($(this).data('time'));
        $(this).addClass('btn-success');
    });
}

function bindSendForm() {
    $('.save-order-btn').unbind();
    $('.save-order-btn').click(function (e) {
        e.preventDefault();

        $.ajax({
            url: "/order/save",
            method: 'post',
            data: {_csrf: csrfToken},
            success: function (response) {
               if (response.success){
                   $("#orderModal").modal('hide');
                   $("#orderSuccessModal").modal('show');
               }else{
                   toastr.warning(response.message);
               }
            }
        });
    })
}


