$(document).ready(function () {


    var filter = {
        options: [],
        kitchens: [],
        types: [],
        subways: [],
        raions: []
    };

    loadFilter();

    function loadFilter() {
        $.ajax({
            method: 'post',
            url: '/catalog/get-filter',
            success: function (response) {
                var filterItems = response.filter;
                renderFilter(filterItems);

                $('.catalog_filter_item').on('change', function () {
                    filter.options = [];
                    filter.kitchens = [];
                    filter.types = [];
                    filter.raions = [];
                    filter.subways = [];
                    $('.catalog_filter_item').each(function (item) {
                        if ($(this).prop('checked')) {
                            switch ($(this).data('type')) {
                                case "option": {
                                    filter.options.push($(this).val());
                                    break;
                                }
                                case 'kitchen': {
                                    filter.kitchens.push($(this).val());
                                    break;
                                }
                                case 'type': {
                                    filter.types.push($(this).val());
                                    break;
                                }
                                case 'subway': {
                                    filter.subways.push($(this).val());
                                    break;
                                }
                                case 'raion': {
                                    filter.raions.push($(this).val());
                                    break;
                                }

                            }

                        }
                    });

                    loadRestaurants();

                });

                loadRestaurants();

                return false;
            }

        });
    }


    function getFilterCounts() {
        $.ajax({
            method: 'POST',
            url: '/catalog/get-filter-counts',
            data: filter,
            success: function (response) {

                response.counts.options.map(function (item) {
                    $('#option_' + item.ID).html('(' + item.count + ')');
                });

                response.counts.kitchens.map(function (item) {
                    $('#kitchen_' + item.ID).html('(' + item.count + ')');
                });

                response.counts.types.map(function (item) {
                    $('#type_' + item.ID).html('(' + item.count + ')');
                });

                response.counts.subways.map(function (item) {
                    $('#subway_' + item.ID).html('(' + item.count + ')');
                });
                response.counts.raions.map(function (item) {
                    $('#raion_' + item.ID).html('(' + item.count + ')');
                });
            }

        });
    }

    function loadRestaurants() {
        $.ajax({
            method: 'POST',
            url: '/catalog/get-restaurants',
            data: filter,
            success: function (response) {
                renderRestaurants(response.restaurants);
                getFilterCounts();
            }

        });
    }

    function renderRestaurants(restaurants) {
        var template = _.template($("#restaurant_tpl").html());
        $(".restaurant-list-wrapper").empty();
        restaurants.map(function (item) {

            $(".restaurant-list-wrapper").append(template(item));


        });
    }

    function renderFilter(filterItems) {

        var template = _.template($("#checkbox_tpl").html());

        var tab_template = _.template($("#tab_tpl").html());


        filterItems.options.map(function (item) {
            $(".options_wrapper").append(template(item));
        });

        filterItems.kitchens.map(function (item) {
            $(".kitchens_wrapper").append(template(item));
        });

        filterItems.types.map(function (item) {
            $(".types_wrapper").append(template(item));
        });

        filterItems.lines.map(function (item) {
            $(".lines_wrapper").append(tab_template(item));

            $('.subway_list').append('<div id="tab_line_' + item.ID + '" class="tab-pane fade"><div class="panel-body line_tab line_' + item.ID + '">');

            var subways = filterItems.subways.filter(function (subway) {
                return subway.LINE == item.ID;
            });

            subways.map(function (subway) {
                $(".line_" + subway.LINE).append(template(subway));
            });

            $('.subway_list').append('</div></div>');
        });

        $('.lines_wrapper').children().first().find('a').trigger('click');

        filterItems.raions.map(function (item) {
            $(".raion_wrapper").append(template(item));
        });

    }


});