$(document).ready(function () {

    $('.new-restaurants-carousel, .restaurants-with-discount-carousel, .popular-restaurants-carousel').owlCarousel({
        margin: 10,
        loop: true,
        dots: false,
        nav: false,
        lazyLoad: true,
        responsive: {
            0: {
                items: 1
            },
            500: {
                items: 2
            },
            800: {
                items: 2
            },
            1000: {
                items: 3
            }
        }
    });

    $('.news-carousel, .topics-carousel').owlCarousel({
        margin: 5,
        loop: true,
        dots: true,
        nav: false,
        lazyLoad: true,
        responsive: {
            0: {
                items: 1
            },
            500: {
                items: 2
            },
            800: {
                items: 2
            },
            1000: {
                items: 3
            }
        }
    });

    $('.special-offer-carousel').owlCarousel({
        margin: 10,
        loop: true,
        dots: true,
        nav: false,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        lazyLoad: true,
        responsive: {
            0: {
                items: 1
            },
            500: {
                items: 2
            },
            800: {
                items: 2
            },
            1000: {
                items: 2
            }
        }
    });

    $('.reston-recomended-topics-carousel').owlCarousel({
        margin: 0,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        animateOut: 'slideOutDown',
        animateIn: 'slideInDown',
        loop: true,
        dots: true,
        nav: false,
        lazyLoad: true,
        responsive: {
            0: {
                items: 1
            },
            500: {
                items: 1
            },
            800: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });


});


$(document).ready(function () {
    var mainTitle = $(".main-title");
    var mainTitlePos = $(".main-title").position();
    var subTitle = $(".subtitle-text");
    var subTitlePos = $(".subtitle-text").position();

    var padding = 10; //Padding set to the header
    var left = mainTitlePos.left + padding;
    var top = mainTitlePos.top + padding;
    mainTitle.find("span").css("background-position", "-" + left + "px -" + top + "px");

    var left = subTitlePos.left + padding;
    var top = subTitlePos.top + padding;
    subTitle.find("span").css("background-position", "-" + left + "px -" + top + "px");
})


$(document).ready(function () {

    $('.reston-recomended-item').each(function (item, value) {
        var base = $(value).find('.base');
        base.addClass('base_' + item);

        var cta = $(value).find('.cta');
        cta.addClass('cta_' + item);


        base.clone().addClass('overlay').addClass('overlay_' + item).appendTo($(value));

        var overlay = $(value).find('.overlay');
        overlay.addClass('overlay_' + item);

        $('.cta_' + item).hover(function () {
            var width = $(this).width();
            $(this).width(width);
            $('.overlay_' + item).toggleClass('over');
        });
    });


});

