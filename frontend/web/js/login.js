$('.sign-up-modal-btn').on('click', function (e) {
    e.preventDefault();
    $("#loginModal").modal('hide');
});


$('.login-modal-btn').on('click', function (e) {
    e.preventDefault();
    $("#registrationModal").modal('hide');
});


$('.login-btn').click(function (e) {

    e.preventDefault();
    var csrfToken = $('meta[name=csrf-token]').attr("content");

    var email = $('input[name="LoginForm[email]"]').val();
    var password = $('input[name="LoginForm[password]"]').val();

    $.ajax({
        url: "/user/user/login",
        method: 'post',
        data: {email: email, password: password, _csrf: csrfToken},
        success: function (response) {
            if (response.success) {
                toastr.options.onHidden = function () {
                    location.reload();
                }
                toastr.success(response.message, null, {timeOut: 2000});

            } else {
                toastr.warning(response.message);
            }

        }
    });

    return false;

});

$('.sign-up-btn').click(function (e) {

    e.preventDefault();
    var csrfToken = $('meta[name=csrf-token]').attr("content");

    var email = $('input[name="SignUpForm[email]"]').val();
    var password = $('input[name="SignUpForm[password]"]').val();
    var phone = $('input[name="SignUpForm[phone]"]').val();
    var userName = $('input[name="SignUpForm[userName]"]').val();
    var password_repeat = $('input[name="SignUpForm[password_repeat]"]').val();


    $.ajax({
        url: "/user/user/sign-up",
        method: 'post',
        data: {
            email: email,
            password: password,
            phone: phone,
            userName: userName,
            password_repeat: password_repeat,
            _csrf: csrfToken
        },
        success: function (response) {
            if (response.success) {
                toastr.options.onHidden = function () {
                    location.reload();
                }
                toastr.success(response.message, null, {timeOut: 2000});

            } else {
                for (var field in  response.message)

                    response.message[field].map(function (message) {
                        toastr.warning(message);
                    })


            }

        }
    });

    return false;

});


