<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 10.03.19
 * Time: 13:25
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class OwlAsset extends AssetBundle
{

    public $sourcePath = '@bower/owl-carousel2';
    public $css = [
        'dist/assets/owl.carousel.min.css',
        'dist/assets/owl.theme.green.min.css'
    ];
    public $js = [
        'dist/owl.carousel.min.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];

}