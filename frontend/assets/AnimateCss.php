<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 10.03.19
 * Time: 13:25
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class AnimateCss extends AssetBundle
{

    public $sourcePath = '@bower/animate.css';
    public $css = [
        'animate.min.css',
    ];
}