<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use frontend\widgets\search\Search;

?>
<div class="container">
    <?= $this->render('/layouts/search_top_panel'); ?>

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#restaurants">Заведения <span class="badge"><?= isset($results['restaurants']) ? count($results['restaurants']) : 0; ?></span></a></li>
        <li><a data-toggle="tab" href="#news">Новости <span class="badge"><?= isset($results['news']) ? count($results['news']) : 0;?></span></a></li>
        <li><a data-toggle="tab" href="#topics">Обзоры <span class="badge"><?= isset($results['topics']) ? count($results['topics']) : 0; ?></span></a></li>
    </ul>

    <div class="tab-content">
        <div id="restaurants" class="tab-pane fade in active">
            <br>
            <?php if (!empty($results['restaurants'])): ?>
                <?php foreach ($results['restaurants'] as $restaurant): ?>
                    <?php /** @var $restaurant \app\models\YiiZavedeniyaLang; */ ?>
                    <?php if (!$restaurant->restaurant) continue; ?>
                    <div class="col-sm-4">
                        <div class="card card-price">
                            <div class="card-img">
                                <a href="#">
                                    <img src="<?= $restaurant->restaurant->getMainImageFile(); ?>"
                                         style="height: 250px; width: 100%"
                                         class="img-responsive">
                                </a>
                            </div>
                            <div class="card-body">
                                <div class="lead">
                                    <a href="#">
                                        <?= $restaurant->NAME; ?>
                                    </a>
                                </div>
                                <ul class="details">
                                    <li style="height: 25px"> <?= $restaurant->ADDRESS; ?></li>
                                    <li><b>Просмотров</b>: <?= $restaurant->restaurant->getViews() ?></li>

                                    <li><b>Средний чек:</b> <?= $restaurant->restaurant->AVG_BILL; ?> грн</li>
                                </ul>
                                <a href="#" data-id="<%= ID %>"
                                   class="btn order-btn btn-success btn-lg btn-block buy-now">
                                    Забронировать
                                </a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <p class="h2 text-danger">Нет результатов</p>
            <?php endif; ?>

        </div>
        <div id="news" class="tab-pane fade">
            <br>
            <?php if (!empty($results['news'])): ?>
                <?php foreach ($results['news'] as $item): ?>
                
                    <div class="col-sm-4">
                        <div class="card" style="height: 500px;">
                            <img class="card-img-top" src="<?= $item->new->getImageFile(); ?>" style="width: 100%; height: 250px"
                                 alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title"><b><?= HTML::encode($item->TITLE); ?></b></h5>
                                <p class="card-text"><?= HtmlPurifier::process($item->SHORT_TEXT); ?></p>
                                <a href="<?= Url::to(['/news/view', 'slug' => $item->new->SLUG]) ?>"
                                   class="btn btn-primary">Подробнее</a>
                            </div>
                        </div>
                    </div>

                <?php endforeach; ?>
            <?php else: ?>
                <p class="h2 text-danger">Нет результатов</p>
            <?php endif; ?>

        </div>
        <div id="topics" class="tab-pane fade">
            <br>
            <?php if (!empty($results['topics'])): ?>
                <?php foreach ($results['topics'] as $item): ?>

                    <div class="col-sm-4">
                        <div class="card" style="height: 500px;">
                            <img class="card-img-top" src="<?= $item->topic->getImageFile(); ?>" style="width: 100%; height: 250px"
                                 alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title"><b><?= HTML::encode($item->TITLE); ?></b></h5>
                                <p class="card-text"><?= HtmlPurifier::process($item->SHORT_TEXT); ?></p>
                                <a href="<?= Url::to(['/topics/view', 'slug' => $item->topic->SLUG]) ?>"
                                   class="btn btn-primary">Подробнее</a>
                            </div>
                        </div>
                    </div>

                <?php endforeach; ?>
            <?php else: ?>
                <p class="h2 text-danger">Нет результатов</p>
            <?php endif; ?>
        </div>
    </div>
</div>