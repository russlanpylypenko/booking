<?php

/* @var $this yii\web\View */

/* @var $firstRestaurantsOfWeek \frontend\models\Zavedeniya */
/* @var $text frontend\models\Page */

use frontend\assets\OwlAsset;
use frontend\widgets\search\Search;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\bootstrap\ButtonDropdown;
use frontend\assets\AnimateCss;

AnimateCss::register($this);
OwlAsset::register($this);

$this->title = 'Reston';
?>


<div class="jumbotron header-bg">

    <div class="container">
        <div class="row">
            <div class="col-sm-6 text-left">
                <?= Html::img('@web/img/logo/logo170x61.png', ['alt' => 'sorestOnme', 'class' => 'logo']); ?>

                <div class="city-dropdown-btn">
                    <?php echo ButtonDropdown::widget([
                        'label' => Yii::$app->city->getCurrentCity()->getName(),
                        'dropdown' => [
                            'items' => Yii::$app->city->getLanguageItems(),
                        ],
                    ]); ?>
                </div>

            </div>
            <div class="col-sm-6 text-right">

                <p class="mb-0"><i class="fa fa-phone text-info" aria-hidden="true"></i> <a
                            href="tel:+38 (044) 384 49 34" class="text-white">+38 (044) 384 49 34</a></p>
                <p><a href="tel:+38 (050) 900 34 93" class="text-white">+38 (050) 900 34 93</a></p>

            </div>
        </div>
    </div>

    <br>
    <br>

    <h1 class="main-title text-uppercase"><span> <?= Yii::t('mainPage', 'best restaurants kiev') ?></span></h1>
    <br>
    <p class="lead subtitle-text text-uppercase">
        <span><?= Yii::t('mainPage', 'reserve a table with a discount or a gift in the best places') ?></span></p>

    <div class="container">
        <div class="text-left search-wrapper">
            <div class="row">
                <div class="col-sm-4 col-sm-offset-1">
                    <?= Search::widget() ?>
                </div>
                <div class="col-sm-2 text-center">
                    <div class="subtitle-text text-uppercase and">
                        <span><?= Yii::t('mainPage', 'Или') ?></span>
                    </div>
                </div>
                <div class="col-sm-4">
                    <a href="<?= Url::to(['/catalog']) ?>" class="btn btn-success col-sm-12 text-uppercase">Смотреть все
                        заведения</a>
                </div>
            </div>
        </div>
    </div>

</div>


<div class="container index-container">


    <ul class="nav nav-pills nav-justified main-tabs-buttons">
        <li class="active"><a data-toggle="pill" href="#new-restaurants">Новые заведения</a></li>
        <li><a data-toggle="pill" href="#with-discont">Со скидкой 20 - 50%</a></li>
        <li><a href="#">Ближайшее заведение</a></li>
    </ul>

    <br>

    <div class="tab-content">
        <div id="new-restaurants" class="tab-pane fade in active">
            <div class="owl-carousel owl-theme new-restaurants-carousel">
                <?php foreach ($newRestaurants as $restaurant): ?>
                    <?php /** @var $restaurant \app\models\Zavedeniya; */ ?>
                    <div class="card card-price">
                        <div class="card-img">
                            <a href="#">
                                <img src="<?= $restaurant->getMainImageFile(); ?>" style="height: 250px; width: 100%"
                                     class="img-responsive">
                            </a>
                        </div>
                        <div class="card-body">
                            <div class="lead">
                                <a href="#">
                                    <?= $restaurant->getName(); ?>
                                </a>
                            </div>
                            <ul class="details">
                                <li style="height: 25px"> <?= $restaurant->getAddress(); ?></li>
                                <li><b>Просмотров</b>: <?= $restaurant->getViews() ?></li>

                                <li><b>Средний чек:</b> <?= $restaurant->AVG_BILL; ?> грн</li>
                            </ul>
                            <a href="#" data-id="<?= $restaurant->ID; ?>"
                               class="btn order-btn btn-success btn-lg btn-block buy-now">
                                Забронировать
                            </a>
                        </div>
                    </div>
                <?php endforeach; ?>

            </div>
        </div>
        <div id="with-discont" class="tab-pane fade">
            <br>
            <div class="owl-carousel owl-theme restaurants-with-discount-carousel">
                <?php foreach ($restaurantsWithDiscounts as $restaurant): ?>
                    <?php /** @var $restaurant \app\models\Zavedeniya; */ ?>
                    <div class="card card-price">
                        <div class="card-img">
                            <a href="#">
                                <img src="<?= $restaurant->getMainImageFile(); ?>" style="height: 250px; width: 100%"
                                     class="img-responsive">
                            </a>
                        </div>
                        <div class="card-body">
                            <div class="lead">
                                <a href="#">
                                    <?= $restaurant->getName(); ?>
                                </a>
                            </div>
                            <ul class="details">
                                <li style="height: 25px"> <?= $restaurant->getAddress(); ?></li>
                                <li><b>Просмотров</b>: <?= $restaurant->getViews() ?></li>

                                <li><b>Средний чек:</b> <?= $restaurant->AVG_BILL; ?> грн</li>
                            </ul>
                            <a href="#" data-id="<?= $restaurant->ID; ?>"
                               class="btn order-btn btn-success btn-lg btn-block buy-now">
                                Забронировать
                            </a>
                        </div>
                    </div>
                <?php endforeach; ?>

            </div>
        </div>
    </div>


    <section class="popoular-restaurants">
        <h3 class="text-center text-uppercase">Популярные заведения</h3>
        <br>

        <div class="owl-carousel owl-theme popular-restaurants-carousel">
            <?php foreach ($popularRestaurants as $restaurant): ?>
                <?php /** @var $restaurant \app\models\Zavedeniya; */ ?>
                <div class="card card-price">
                    <div class="card-img">
                        <a href="#">
                            <img src="<?= $restaurant->getMainImageFile(); ?>" style="height: 250px; width: 100%"
                                 class="img-responsive">
                        </a>
                    </div>
                    <div class="card-body">
                        <div class="lead">
                            <a href="#">
                                <?= $restaurant->getName(); ?>
                            </a>
                        </div>
                        <ul class="details">
                            <li style="height: 25px"> <?= $restaurant->getAddress(); ?></li>
                            <li><b>Просмотров</b>: <?= $restaurant->getViews() ?></li>

                            <li><b>Средний чек:</b> <?= $restaurant->AVG_BILL; ?> грн</li>
                        </ul>
                        <a href="#" data-id="<?= $restaurant->ID; ?>"
                           class="btn order-btn btn-success btn-lg btn-block buy-now">
                            Забронировать
                        </a>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>

    </section>

    <div class="row">
        <div class="col-sm-4">
            <section class="top-week-restaurants">
                <h3 class="text-center text-uppercase">Топ заведений недели</h3>
                <br>
                <div class="block">
                    <div class="top text-center">
                        <i class="fa fa-star-o" aria-hidden="true"></i>
                    </div>
                    <p class="h3 text-center"><b>Наш выбор</b></p>

                    <div class="middle">
                        <img class="img-responsive" src="<?= $firstRestaurantsOfWeek->getLogoFile() ?>"
                             alt="<?= $firstRestaurantsOfWeek->lang->NAME ?>"/>
                    </div>
                    <div class="text-center">
                        <a href="<?= Url::to(['/catalog/view', 'slug' => $firstRestaurantsOfWeek->SLUG]) ?>"
                           class="link-black h3"><?= $firstRestaurantsOfWeek->lang->NAME ?></a>
                    </div>
                    <br><br>
                    <div class="bottom">
                        <a href="#" data-id="<?= $firstRestaurantsOfWeek->ID ?>"
                           class="btn order-btn btn-success btn-lg btn-block buy-now">
                            Забронировать
                        </a>
                    </div>
                </div>

                <ul class="list-group">
                    <?php foreach ($restaurantsOfWeek as $restaurant): ?>
                        <?php /** @var $restaurant frontend\models\Zavedeniya; */ ?>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-sm-4">
                                    <img class="img-responsive" src="<?= $restaurant->getMainImageFile(); ?>"
                                         alt="<?= $restaurant->lang->NAME ?>">
                                </div>
                                <div class="col-sm-8">
                                    <p class="h5"><?= $restaurant->lang->NAME ?></p>
                                    <a href="<?= Url::to(['/catalog/view', 'slug' => $firstRestaurantsOfWeek->SLUG]) ?>"
                                       class="text-success">Подробнее</a>
                                </div>
                            </div>
                        </li>
                    <?php endforeach; ?>
                    <li class="list-group-item">
                        <div class="row">
                            <div class="text-center">
                                <a href="<?= Url::to(['/catalog']); ?>" class="text-info">Все заведения</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </section>
        </div>
        <div class="col-sm-8">
            <section class="special-offers">
                <h3 class="text-left text-uppercase">Специальные предложения</h3>
                <br>
                <div class="special-offer-carousel owl-carousel owl-theme">
                    <?php foreach ($specialOffers as $specialOffer): ?>
                        <?php /** @var $specialOffer \frontend\models\Offers */ ?>
                        <div class="panel panel-default">
                            <div class="panel-body p-0">
                                <div class="panelTop">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <img class="img-responsive" src="<?= $specialOffer->getImageFile(); ?>"
                                                 alt="<?= $specialOffer->lang->NAME; ?>"
                                                 title="<?= $specialOffer->lang->NAME; ?>">
                                        </div>
                                        <div class="col-md-7">
                                            <p class="h4"><?= $specialOffer->lang->NAME; ?></p>
                                            <p><?= Yii::$app->formatter->asDatetime($specialOffer->DATE, 'php:Y.m.d') ?></p>

                                            <div class="panelBottom">
                                                <div class="text-left">
                                                    <h5><?= $specialOffer->restaurant->lang->NAME ?></h5>
                                                    <p><i class="fa fa-subway text-success"
                                                          aria-hidden="true"></i> <?= $specialOffer->restaurant->subway->lang->NAME ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="text-center">
                    <a href="<?= Url::to(['/offers']) ?>" class="btn btn-success btn-lg">Все предложения</a>
                </div>
            </section>
            <hr>
            <section class="reston-recomended">
                <h3 class="text-left text-uppercase">Reston рекомендует <i class="fa fa-thumbs-o-up"></i></h3>

                <div class="reston-recomended-topics-carousel owl-carousel owl-theme">
                    <?php foreach ($restonRecomendedTopicParts as $partTopics): ?>
                        <div class="topic-carousel-item">

                            <?php foreach ($partTopics as $topic): ?>
                                <?php /** @var $topic \frontend\models\Topics */ ?>

                                <div class="landing reston-recomended-item">
                                    <div class="base ">
                                        <div class="image">
                                            <img src="<?= $topic->getOriginalImageFile(); ?>"/>
                                        </div>
                                        <div class="copy">
                                            <div class="title"><?= $topic->lang->TITLE; ?></div>
                                            <div class="text"><?= $topic->lang->SHORT_TEXT; ?></div>
                                            <a href="<?= Url::to(['/topics/view', 'slug' => $topic->getSlug()]) ?>"
                                               class="cta">Подробнее</a>
                                        </div>
                                    </div>
                                </div>

                            <?php endforeach; ?>

                        </div>
                    <?php endforeach; ?>
                </div>

            </section>
        </div>
    </div>

</div>

<section class="news-and-reviews">
    <div class="container">
        <div class="text-center">
            <ul class="nav nav-pills inline-block">
                <li class="active"><a data-toggle="pill" href="#topics" class="text-uppercase">Обзоры</a></li>
                <li><a data-toggle="pill" href="#news" class="text-uppercase">Новости</a></li>
            </ul>

            <div class="tab-content">
                <br>
                <div id="topics" class="tab-pane fade in active">

                    <div class="topics-carousel owl-carousel owl-theme">
                        <?php foreach ($topics as $item): ?>
                            <?php /** @var $item frontend\models\Topics; */ ?>

                            <div class="grid">
                                <figure class="effect-roxy">
                                    <img src="<?= $item->getImageFile(); ?>" alt="<?= $item->lang->TITLE ?>"/>
                                    <figcaption>
                                        <h4><?= $item->lang->TITLE ?></h4>
                                        <p><?= Html::encode($item->lang->SHORT_TEXT) ?></p>
                                        <a href="<?= Url::to(['/topics/view/', 'slug' => $item->getSlug()]) ?>">Подробнее</a>
                                        <hr>
                                        <p class="text-center"><?= Yii::$app->formatter->asDatetime($item->DATE, 'php:Y/m/d') ?></p>
                                    </figcaption>
                                </figure>
                            </div>
                        <?php endforeach; ?>
                    </div>

                </div>
                <div id="news" class="tab-pane fade">

                    <div class="news-carousel owl-carousel owl-theme">
                        <?php foreach ($news as $item): ?>
                            <?php /** @var $item frontend\models\News; */ ?>

                            <div class="grid">
                                <figure class="effect-roxy">
                                    <img src="<?= $item->getImageFile(); ?>" alt="<?= $item->lang->TITLE ?>"/>
                                    <figcaption>
                                        <h4><?= $item->lang->TITLE ?></h4>
                                        <p><?= Html::encode($item->lang->SHORT_TEXT) ?></p>
                                        <a href="<?= Url::to(['/news/view/', 'slug' => $item->getSlug()]) ?>">Подробнее</a>
                                        <hr>
                                        <p class="text-center"><?= Yii::$app->formatter->asDatetime($item->DATE, 'php:Y/m/d') ?></p>
                                    </figcaption>
                                </figure>
                            </div>
                        <?php endforeach; ?>
                    </div>

                </div>
            </div>
        </div>

    </div>

</section>

<section class="download-app">
<div class="container">
    <div class="col-sm-8">
        <h3 class="h2">Лучшие столики в ресторанах всегда под рукой</h3>
        <p>Скачайте наше бесплатное приложение для iPhone и Android</p>
        <p>
            <a href="#"><i class="fa fa-play fa-4x text-white"></i></a>
            <a href="#"><i class="fa fa-apple fa-4x text-white"></i></a>
        </p>
    </div>
</div>
</section>
<section class="page-text">
    <div class="container">

            <?= Html::decode($text->lang->VAL);?>

    </div>
</section>


<?php $this->registerJsFile('@web/js/index.js', [
    'depends' => \yii\web\JqueryAsset::className(),
]);

echo $this->render('//catalog/templates/restaurant');

?>


