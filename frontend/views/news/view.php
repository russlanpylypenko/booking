<?php
/* @var $this yii\web\View */
/* @var $article \app\models\News */
use yii\helpers\HtmlPurifier;
?>
<div class="container">
    <h1><?= HtmlPurifier::process($article->lang->getTitle());?></h1>

    <p>
        <?= html_entity_decode($article->lang->getText()) ;?>
    </p>
</div>

