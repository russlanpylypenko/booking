<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
?>
<div class="container">
    <?= $this->render('/layouts/search_top_panel'); ?>
    <h1>Новости</h1>

    <div class="row">
        <?php foreach ($newsList as $new): ?>
            <?php /** @var $new \app\models\News; */ ?>
            <div class="col-sm-4">
                <div class="card" style="height: 450px;">
                    <img class="card-img-top" src="<?= $new->getImageFile(); ?>" style="width: 100%; height: 250px"
                         alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title"><b><?= HTML::encode($new->lang->getTitle()); ?></b></h5>
                        <p class="card-text"><?= HTML::encode($new->DATE); ?></p>
                        <p class="card-text"><?= HTML::encode($new->lang->getShortText()); ?></p>
                        <a href="<?= Url::to(['/news/view', 'slug' => $new->getSlug()])?>" class="btn btn-primary">Подробнее</a>
                    </div>
                </div>
            </div>

        <?php endforeach; ?>
    </div>

    <div class="row">
        <?php echo LinkPager::widget([
            'pagination' => $pages,
        ]);
        ?>
    </div>
</div>


