<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use frontend\assets\UnderscoreAsset;
use frontend\assets\ToastrAsset;
use yii\helpers\Url;
use frontend\widgets\user\UserAuth;
use frontend\widgets\user\SignUp;
use frontend\widgets\order\Order;
use frontend\assets\FontAwesomeAsset;

AppAsset::register($this);
UnderscoreAsset::register($this);
ToastrAsset::register($this);
FontAwesomeAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php


    NavBar::begin([
        'brandLabel' => Html::img('@web/img/logo/logo142x51.png', ['alt'=>'sorestOnme', 'class'=>'navbar-logo']),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-default navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' =>  Yii::t('mainMenu', 'catalog'), 'url' => ['/catalog/index']],
        ['label' => Yii::t('mainMenu', 'banquets'), 'url' => ['/banquets/index']],
        ['label' => Yii::t('mainMenu', 'news'), 'url' => ['/news/index']],
        ['label' => Yii::t('mainMenu', 'topics'), 'url' => ['/topics/index']],
        ['label' => Yii::t('mainMenu', 'affiche'), 'url' => ['/affiche/index']],
    ];
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => $menuItems,
    ]);


    $menuRight = [
        [
            'label' => '<span class="glyphicon glyphicon-user text-info"></span>',
            'url' => Url::to(['/user/profile/view']),
            'linkOptions' => ['data-method' => 'post'],
            'visible' => !Yii::$app->user->isGuest
        ],
        [
            'label' => '<span class="glyphicon glyphicon-log-out"></span>',
            'url' => Url::to(['/user/user/logout']),
            'linkOptions' => ['data-method' => 'post'],
            'visible' => !Yii::$app->user->isGuest
        ],
        [
            'label' => '<span class="glyphicon glyphicon-log-in" data-toggle="modal" data-target="#loginModal"></span>',
            'visible' => Yii::$app->user->isGuest,
            'linkOptions' => ['class' => 'navbar-popup-btn'],
        ],
        [
            'label' => Yii::$app->languageDropdown->getCurrentLanguageItem(),
            'items' => Yii::$app->languageDropdown->getLanguageItems(),
            'linkOptions' => ['class' => 'navbar-language-selector'],
        ],


    ];

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'encodeLabels' => false,
        'items' => $menuRight,
    ]);

    NavBar::end();
    ?>


    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <?= Alert::widget() ?>

    <?= $content ?>

</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?= Order::widget() ?>
<?= UserAuth::widget() ?>
<?= SignUp::widget() ?>

<?php
$this->registerJsFile('@web/js/order.js', [
    'depends' => \yii\bootstrap\BootstrapAsset::className(),
]);

$this->registerJsFile('@web/js/login.js', [
    'depends' => \yii\bootstrap\BootstrapAsset::className(),
]);
?>

<?php $this->endBody() ?>


</body>
</html>
<?php $this->endPage() ?>
