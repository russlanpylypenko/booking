<?php

use yii\bootstrap\ButtonDropdown;
use yii\helpers\Html;
use frontend\widgets\search\Search;

?>

<div class="well">
    <div class="row">
        <div class="col-sm-3">
            <?= Html::img('@web/img/logo/logo142x51.png', ['alt' => 'sorestOnme', 'class' => 'logo']); ?>

            <div class="city-dropdown-btn">
                <?php echo ButtonDropdown::widget([
                    'label' => Yii::$app->city->getCurrentCity()->getName(),
                    'dropdown' => [
                        'items' => Yii::$app->city->getLanguageItems(),
                    ],
                ]); ?>
            </div>
        </div>
        <div class="col-sm-6 text-center">
            <?= Search::widget() ?>
        </div>
        <div class="col-sm-3 text-right">
            <p class="mb-0"><i class="fa fa-phone text-info" aria-hidden="true"></i> <a
                        href="tel:+38 (044) 384 49 34">+38 (044) 384 49 34</a></p>
            <p><a href="tel:+38 (050) 900 34 93">+38 (050) 900 34 93</a></p>
        </div>
    </div>

</div>