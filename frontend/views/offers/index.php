<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
?>
<div class="container">
    <h1>Предложения</h1>

    <div class="row">
        <?php foreach ($offerList as $offer): ?>
            <?php /** @var  $offer \app\models\Offers; */ ?>
            <div class="col-sm-4">
                <div class="card" style="height: 450px;">
                    <img class="card-img-top" src="https://picsum.photos/250" style="width: 100%; height: 250px"
                         alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title"><b><?= HTML::encode($offer->lang->getTitle()); ?></b></h5>
                        <p class="card-text"><?= HTML::encode($offer->lang->getShortText()); ?></p>
                        <a href="<?= Url::to(['/topic/view', 'slug' => $offer->getSlug()])?>" class="btn btn-primary">Подробнее</a>
                    </div>
                </div>
            </div>

        <?php endforeach; ?>
    </div>

    <div class="row">
        <?php echo LinkPager::widget([
            'pagination' => $pages,
        ]);
        ?>
    </div>

</div>

