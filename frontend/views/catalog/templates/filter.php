<script type="text/template" id="checkbox_tpl">
    <div class="checkbox">
        <label><input type="checkbox" class="catalog_filter_item" data-type="<%= type %>" name="<%= type %>[]" value="<%= ID %>"> <%= NAME %> <span id="<%= type + '_' + ID %>"></span> </label>
    </div>
</script>

<script type="text/template" id="tab_tpl">
    <li class=""><a data-toggle="tab" href="#tab_line_<%= ID %>" class="btn btn-default"> <%= NAME %></a></li>
</script>

