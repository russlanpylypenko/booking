<script type="text/template" id="restaurant_tpl">
    <div class="col-sm-6">
        <div class="card card-price">
            <div class="card-img">
                <a href="#">
                    <img src=" <%= img %>" style="height: 250px; width: 100%"
                         class="img-responsive">
                </a>
            </div>
            <div class="card-body">
                <div class="lead">
                    <a href="#">
                        <%= lang.NAME %>
                    </a>
                </div>
                <ul class="details">
                    <li style="height: 25px"><%= lang.ADDRESS %></li>
                    <li><b>Просмотров</b>: <%= VIEWS %></li>

                    <li><b>Средний чек:</b> <%= AVG_BILL %> грн</li>
                </ul>
                <a href="#" data-id="<%= ID %>" class="btn order-btn btn-success btn-lg btn-block buy-now">
                    Забронировать
                </a>
            </div>
        </div>
    </div>
</script>
