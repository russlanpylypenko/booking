<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="container">

    <?= $this->render('/layouts/search_top_panel'); ?>

    <h1>Каталог Заведений</h1>

    <hr>

    <div class="col-sm-3">

        <?= Html::beginForm(['/catalog'], 'post', ['data-pjax' => '', 'class' => 'filter-catalog',]); ?>
        <div class="panel-group">
            <div class="panel panel-default">
                <div class="panel-heading">Метро</div>

                <ul class="nav nav-tabs lines_wrapper">

                </ul>

                <div class="tab-content subway_list">


                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Район</div>
                <div class="panel-body raion_wrapper filter_items_list">


                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Тип заведения</div>
                <div class="panel-body types_wrapper filter_items_list">


                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Кухня</div>
                <div class="panel-body kitchens_wrapper filter_items_list">


                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Опции</div>
                <div class="panel-body options_wrapper filter_items_list">


                </div>
            </div>


        </div>
        <?= Html::endForm() ?>
    </div>
    <div class="col-sm-9">

        <div class="row">
            <div class="restaurant-list-wrapper">

            </div>
        </div>

    </div>
</div>


<?php $this->registerJsFile('@web/js/catalog.js', [
    'depends' => \yii\web\JqueryAsset::className(),
]);

echo $this->render('templates/filter');
echo $this->render('templates/restaurant');

?>
