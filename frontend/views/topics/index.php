<?php
/* @var $this yii\web\View */

use Yii;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;

?>

<div class="container">
    <?= $this->render('/layouts/search_top_panel'); ?>
    <h1>Обзоры</h1>


    <div class="row">
        <div class="col-sm-4">
            <h4>Последние новости</h4>
            <ul class="list-group">
                <?php foreach ($newsList as $item): ?>
                    <a href="<?= Url::to(['/news/view/', 'slug' => $item['SLUG']]) ?>" class="list-group-item">
                        <?= $item['lang']['TITLE'] ?>
                        <br>
                        <small> <?= Yii::$app->formatter->asDate($item['DATE'], 'long') ?> </small>
                    </a>
                <?php endforeach; ?>
            </ul>
        </div>
        <?php foreach ($topicList as $topic): ?>
            <?php /** @var $topic \frontend\models\Topics; */ ?>
            <div class="col-sm-4">
                <div class="card" style="height: 450px;">
                    <img class="card-img-top" src="<?= $topic->getImageFile() ?>" style="width: 100%; height: 240px"
                         alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title"><b><?= HTML::encode($topic->lang->getTitle()); ?></b></h5>
                        <p class="card-text"><?= HTML::encode($topic->lang->getShortText()); ?></p>
                        <a href="<?= Url::to(['/topic/view', 'slug' => $topic->getSlug()]) ?>" class="btn btn-primary">Подробнее</a>
                    </div>
                </div>
            </div>

        <?php endforeach; ?>
    </div>

    <div class="row">
        <?php echo LinkPager::widget([
            'pagination' => $pages,
        ]);
        ?>
    </div>

</div>



