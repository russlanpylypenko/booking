<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="container">
    <h1>Aфиша</h1>

    <?php foreach ($afficheList as $affiche): ?>
        <?php if ($affiche['type'] === 'offer'): ?>
            <div class="col-sm-2">
                <div class="card" style="height: 450px;">
                    <img class="card-img-top" src="https://picsum.photos/250" style="width: 100%; height: 250px"
                         alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title"><b><?= HTML::encode($affiche['lang']['NAME']); ?></b></h5>
                        <!--                <p class="card-text">-->
                        <? //= HTML::encode($affiche['lang']['SHORT_']); ?><!--</p>-->
                        <a href="<?= Url::to(['/topic/view', 'slug' => $affiche['SLUG']]) ?>"
                           class="btn btn-primary">Подробнее</a>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if ($affiche['type'] === 'news'): ?>
            <div class="col-sm-4">
                <div class="card" style="height: 450px;">
                    <img class="card-img-top" src="https://picsum.photos/250" style="width: 100%; height: 250px"
                         alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title"><b><?= HTML::encode($affiche['lang']['TITLE']); ?></b></h5>
                        <p class="card-text"><?= HTML::encode($affiche['lang']['SHORT_TEXT']); ?></p>
                        <a href="<?= Url::to(['/topic/view', 'slug' => $affiche['SLUG']]) ?>"
                           class="btn btn-primary">Подробнее</a>
                    </div>
                </div>
            </div>
        <?php endif; ?>

    <?php endforeach; ?>
</div>
