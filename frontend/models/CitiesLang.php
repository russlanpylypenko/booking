<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "yii_cities_lang".
 *
 * @property int $ID
 * @property int $CITY_ID
 * @property string $NAME
 * @property string $LANG
 */
class CitiesLang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_cities_lang';
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'CITY_ID' => 'City ID',
            'NAME' => 'Name',
            'LANG' => 'Lang',
        ];
    }
}
