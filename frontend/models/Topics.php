<?php

namespace frontend\models;

use frontend\components\LanguageHelper;
use Yii;

/**
 * This is the model class for table "yii_topics".
 *
 * @property int $ID
 * @property string $DATE
 * @property string $IMAGE
 * @property string $MAIN_IMAGE
 * @property int $SHOW_MAIN_IMAGE
 * @property int $ZAVEDENIYE_ID
 * @property string $SLUG
 * @property int $VIEWS
 * @property int $CITY
 * @property int $TOPICS_TYPE_ID
 * @property int $TOPICS_AUTHOR_ID
 * @property int $IS_PAINTED_LABEL
 * @property int $FIXED_MAIN
 * @property string $FIXED_MAIN_DATE
 * @property int $RESTON_RECOMENDED_ORDER
 * @property int $TEST_ID
 */
class Topics extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_topics';
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'DATE' => 'Date',
            'IMAGE' => 'Image',
            'MAIN_IMAGE' => 'Main Image',
            'SHOW_MAIN_IMAGE' => 'Show Main Image',
            'ZAVEDENIYE_ID' => 'Zavedeniye ID',
            'SLUG' => 'Slug',
            'VIEWS' => 'Views',
            'CITY' => 'City',
            'TOPICS_TYPE_ID' => 'Topics Type ID',
            'TOPICS_AUTHOR_ID' => 'Topics Author ID',
            'IS_PAINTED_LABEL' => 'Is Painted Label',
            'FIXED_MAIN' => 'Fixed Main',
            'FIXED_MAIN_DATE' => 'Fixed Main Date',
            'RESTON_RECOMENDED_ORDER' => 'Reston Recomended Order',
            'TEST_ID' => 'Test ID',
        ];
    }

    public function getSlug()
    {
        return $this->SLUG ? $this->SLUG : $this->ID;
    }


    public function getLang()
    {
        return $this->hasOne(TopicsLang::className(), ['TOPIC_ID' => 'ID'])->where(['LANG' => LanguageHelper::getLangCode()]);
    }

    public function getImageFile()
    {
        $Image = $this->IMAGE;

        $src = $Image ? '/uploads/img/topics/' . $Image : 'https://picsum.photos/175';
        return $src;
    }

    public function getOriginalImageFile()
    {
        $Image = $this->IMAGE;

        $src = $Image ? '/uploads/img/topics/original/' . $Image : 'https://picsum.photos/475';
        return $src;
    }
}
