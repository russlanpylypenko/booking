<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 12.03.19
 * Time: 20:49
 */

namespace frontend\models\forms;

use Yii;
use app\models\Orders;
use yii\base\Model;

class OrderForm extends Model
{
    public $date;
    public $time;
    public $persons;
    public $customer_name;
    public $customer_phone;
    public $comment;

    public function rules()
    {
        return [
            [['date', 'persons', 'time', 'customer_phone'], 'required'],
            [['persons'], 'in', 'range' => [1, 2, 3]],
            [['time'], 'match', 'pattern' => '/^\d\d:\d\d$/'],

        ];
    }



    public function getPeopleCountList()
    {
        return [
            0 => '',
            2 => "2 человека",
            3 => "3 человека",
            4 => "4 человека",
            5 => "5 человек",
            6 => "6 человек",
        ];
    }


    public function save()
    {
        Yii::$app->emailService->notifyAdmins('Order added');
        return true;
    }




}