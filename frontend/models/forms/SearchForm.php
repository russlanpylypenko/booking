<?php

namespace frontend\models\forms;

use frontend\models\SearchModel;

class SearchForm extends \yii\base\Model
{
    public $q;

    public function rules()
    {
        return [
            ['q', 'trim'],
            ['q', 'required'],
            ['q', 'string', 'min' => 3],
        ];
    }

    public function search()
    {
        if ($this->validate()) {
            $model = new SearchModel();
            return $model->fulltextSearch($this->q);
        }
    }

}