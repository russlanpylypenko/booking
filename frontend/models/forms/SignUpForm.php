<?php

namespace frontend\models\forms;

use app\models\Profile;
use frontend\models\User;
use yii\base\Model;

class SignUpForm extends Model
{

    const ADMIN = 1; // Администратор RestOn
    const ADMIN_CLIENT = 2; // Администратор Client
    const USER = 4; // Пользователь
    const USER_FB = 5; // ФБ Пользователь
    const PORTAL_OWNER = 6; // Владелец портала
    const MANAGER_CLIENT = 7; // Менеджер Client


    public $userName;
    public $email;
    public $phone;
    public $password;
    public $password_repeat;

    public function rules()
    {
        return [
            [['email', 'password', 'password_repeat', 'phone', 'userName'], 'required'],
            ['email', 'email'],
            ['password', 'string', 'min' => 5],
            ['password_repeat', 'compare', 'compareAttribute' => 'password'],
            ['email', 'unique', 'targetClass' => '\frontend\models\User', 'message' => 'Такой email уже существует'],
        ];
    }


    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->EMAIL = $this->email;
        $user->LOGIN = $this->email;
        $user->IP = $_SERVER['REMOTE_ADDR'];
        $user->CONFIRMED = 0;
        $user->CONFIRM_KEY = "";
        $user->ZAVEDENIE_ID = 0;
        $user->auth_key = '';
        $user->ROLE = self::USER;
        $user->SUBSCRIBE = 1;
        $user->DATE_REG = $time = time();
        $user->LAST_VISIT = $time;
        $user->REF = 0;
        $user->PHOTO = ' ';
        $user->FB = 0;
        $user->VK = 0;
        $user->setPassword($this->password);
        $user->save();

        if($user){
            $list = explode(' ', trim($this->userName), 2);
            $fname = $list[0];
            if(count($list) == 2) { $lname = $list[1]; } else{ $lname = '';}

            $profile = new Profile();
            $profile->USER_ID = $user->ID;
            $profile->LNAME = $lname;
            $profile->FNAME = $fname;
            $profile->SNAME = '';
            $profile->GENDER = '';
            $profile->BIRTH_DATE = date("Y-m-d");
            $profile->FPHONE = $this->phone;
            $profile->ICQ = 0;
            $profile->SKYPE = 0;
            $profile->ADDRESS = '';
            $profile->CART = '';
            $profile->CITY = '';
            $profile->COMMENT = '';
            $profile->save();
        }

        return $user ? $user : null;
    }
}