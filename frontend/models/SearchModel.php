<?php

namespace frontend\models;

use Yii;
use yii\helpers\ArrayHelper;
use frontend\components\LanguageHelper;

class SearchModel
{

    public function fulltextSearch($keyword)
    {

        $limit = Yii::$app->params['searchLimit'];

        $sql = "SELECT * FROM idx_booking_restaurants WHERE MATCH('$keyword') OPTION ranker=WORDCOUNT";
        $data = Yii::$app->sphinx->createCommand($sql)->queryAll();
        $ids = ArrayHelper::map($data, 'id', 'id');

        $data = YiiZavedeniyaLang::find()
            ->with([
                'restaurant' => function ($query) use ($ids) {
                    $query->select('ID, VISIBLE, AVG_BILL, VIEWS, MAIN_IMAGE_ID')
                        ->orderBy(['VIEWS' => SORT_DESC])->where(['VISIBLE' => 1])->andWhere(["AVAILABLE" => 1]);
                }
            ])
            ->select('ID, NAME, ADDRESS, ZAVEDENIYE_ID, LANG')
            ->where(['ID' => $ids])
            ->andWhere(['LANG' => LanguageHelper::getLangCode()])
            ->limit($limit)
            ->all();
        $data = ArrayHelper::index($data, 'ID');

       // var_dump($data);
        $restaurants = [];
        foreach ($ids as $element){
            if(isset($data[$element]) && $data[$element]->restaurant){
                $restaurants[] = $data[$element];
            }
        }


        $sql = "SELECT * FROM idx_booking_news WHERE MATCH('$keyword') OPTION ranker=WORDCOUNT";
        $data = Yii::$app->sphinx->createCommand($sql)->queryAll();
        $ids = ArrayHelper::map($data, 'id', 'id');
        $data = NewsLang::find()->with([
            'new' => function ($query) {
                $query->orderBy(['DATE' => SORT_DESC])
                    ->where("DATE < '" . date('Y-m-d H:i') . "'")
                    ->orWhere("DATE_END > '" . date('Y-m-d H:i') . "'")
                    ->andWhere("DATE_END = '0000-00-00'");
            }
        ])->where(['ID' => $ids])->andWhere(['LANG' => LanguageHelper::getLangCode()])->limit($limit)->all();
        $data = ArrayHelper::index($data, 'ID');
        $news = [];

        foreach ($ids as $element){
            if(isset($data[$element]) && isset($data[$element]['new'])){
                $news[] = $data[$element];
            }
        }


        $sql = "SELECT * FROM idx_booking_topics WHERE MATCH('$keyword') OPTION ranker=WORDCOUNT";
        $data = Yii::$app->sphinx->createCommand($sql)->queryAll();
        $ids = ArrayHelper::map($data, 'id', 'id');
        $data = TopicsLang::find()->with([
            'topic' => function ($query) {
                $query->orderBy(['DATE' => SORT_DESC])
                    ->where("DATE < '" . date('Y-m-d H:i') . "'");
            }
        ])->where(['ID' => $ids])->andWhere(['LANG' => LanguageHelper::getLangCode()])->limit($limit)->all();
        $data = ArrayHelper::index($data, 'ID');
        $topics = [];

        foreach ($ids as $element){
            if(isset($data[$element]) && isset($data[$element]['topic'])){
                $topics[] = $data[$element];
            }
        }

        return [
            'restaurants' => $restaurants,
            'news' => $news,
            'topics' => $topics
        ];
    }

}