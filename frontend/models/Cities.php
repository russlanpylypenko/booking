<?php

namespace app\models;

use frontend\components\LanguageHelper;
use Yii;

/**
 * This is the model class for table "yii_cities".
 *
 * @property int $ID
 * @property string $SLUG
 *
 * @property YiiZavedeniya[] $yiiZavedeniyas
 */
class Cities extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_cities';
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'SLUG' => 'Slug',
        ];
    }


    public function getLang()
    {
        return $this->hasOne(CitiesLang::className(), ['CITY_ID' => 'ID'])->where(['LANG' => LanguageHelper::getLangCode()]);
    }

    public function getName()
    {
        return $this->lang ? $this->lang->NAME : "Нет";
    }
}
