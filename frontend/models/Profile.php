<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "yii_profile".
 *
 * @property int $ID
 * @property int $USER_ID
 * @property string $LNAME
 * @property string $FNAME
 * @property string $SNAME
 * @property string $GENDER
 * @property string $BIRTH_DATE
 * @property string $FPHONE
 * @property int $PHONE_COMFIRMED
 * @property string $ICQ
 * @property string $SKYPE
 * @property string $ADDRESS
 * @property string $CART
 * @property string $CITY
 * @property string $COMMENT
 * @property int $BINOTEL_PHONE_LINE
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_profile';
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'USER_ID' => 'User ID',
            'LNAME' => 'Lname',
            'FNAME' => 'Fname',
            'SNAME' => 'Sname',
            'GENDER' => 'Gender',
            'BIRTH_DATE' => 'Birth Date',
            'FPHONE' => 'Fphone',
            'PHONE_COMFIRMED' => 'Phone Comfirmed',
            'ICQ' => 'Icq',
            'SKYPE' => 'Skype',
            'ADDRESS' => 'Address',
            'CART' => 'Cart',
            'CITY' => 'City',
            'COMMENT' => 'Comment',
            'BINOTEL_PHONE_LINE' => 'Binotel Phone Line',
        ];
    }

    public function getName(){
        return $this->FNAME . " " . $this->LNAME;
    }
}
