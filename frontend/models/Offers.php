<?php

namespace frontend\models;

use frontend\components\LanguageHelper;
use Yii;

/**
 * This is the model class for table "yii_offers".
 *
 * @property int $ID
 * @property string $IMAGE
 * @property string $DATE
 * @property string $DATE_START
 * @property string $DATE_END
 * @property int $ZAVEDENIYE_ID
 * @property int $FIXED_MAIN
 * @property int $FIXED_AFFICHE
 * @property string $SLUG
 * @property int $CITY
 */
class Offers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_offers';
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'IMAGE' => 'Image',
            'DATE' => 'Date',
            'DATE_START' => 'Date Start',
            'DATE_END' => 'Date End',
            'ZAVEDENIYE_ID' => 'Zavedeniye ID',
            'FIXED_MAIN' => 'Fixed Main',
            'FIXED_AFFICHE' => 'Fixed Affiche',
            'SLUG' => 'Slug',
            'CITY' => 'City',
        ];
    }

    public function getSlug()
    {
        return $this->SLUG ? $this->SLUG : $this->ID;
    }


    public function getLang()
    {
        return $this->hasOne(OffersLang::className(), ['OFFER_ID' => 'ID'])->where(['LANG' => LanguageHelper::getLangCode()]);
    }

    public function getRestaurant()
    {
        return $this->hasOne(Zavedeniya::className(), ['ID' => 'ZAVEDENIYE_ID']);
    }

    public function getImageFile()
    {
        $Image = $this->IMAGE;

        $src = $Image ? '/uploads/img/offers/' . $Image : 'https://picsum.photos/175';
        return $src;
    }
}
