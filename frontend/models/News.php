<?php

namespace frontend\models;

use Yii;
use frontend\components\LanguageHelper;

/**
 * This is the model class for table "yii_news".
 *
 * @property int $ID
 * @property string $DATE
 * @property string $PUBLICATION_DATE
 * @property string $DATE_START
 * @property string $DATE_END
 * @property string $IMAGE
 * @property int $ZAVEDENIYE_ID
 * @property string $SLUG
 * @property int $CITY
 * @property int $FIXED_AFFICHE
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_news';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'DATE' => 'Date',
            'PUBLICATION_DATE' => 'Publication Date',
            'DATE_START' => 'Date Start',
            'DATE_END' => 'Date End',
            'IMAGE' => 'Image',
            'ZAVEDENIYE_ID' => 'Zavedeniye ID',
            'SLUG' => 'Slug',
            'CITY' => 'City',
            'FIXED_AFFICHE' => 'Fixed Affiche',
        ];
    }

    public function getSlug()
    {
        return $this->SLUG ? $this->SLUG : $this->ID;
    }


    public function getLang()
    {
        return $this->hasOne(NewsLang::className(), ['NEWS_ID' => 'ID'])->where(['LANG' => LanguageHelper::getLangCode()]);
    }

    public function getImageFile()
    {
        $Image = $this->IMAGE;

        $src = $Image ? '/uploads/img/news/' . $Image : 'https://picsum.photos/175';
        return $src;
    }
}
