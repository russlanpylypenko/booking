<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "yii_subway_lang".
 *
 * @property int $ID
 * @property int $SUBWAY_ID
 * @property string $NAME
 * @property string $LANG
 */
class SubwayLang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_subway_lang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SUBWAY_ID'], 'required'],
            [['SUBWAY_ID'], 'integer'],
            [['NAME'], 'string', 'max' => 100],
            [['LANG'], 'string', 'max' => 2],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'SUBWAY_ID' => 'Subway ID',
            'NAME' => 'Name',
            'LANG' => 'Lang',
        ];
    }
}
