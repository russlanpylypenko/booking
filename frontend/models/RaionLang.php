<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "yii_raion_lang".
 *
 * @property int $ID
 * @property int $RAION_ID
 * @property string $NAME
 * @property string $LANG
 */
class RaionLang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_raion_lang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['RAION_ID'], 'required'],
            [['RAION_ID'], 'integer'],
            [['NAME'], 'string', 'max' => 100],
            [['LANG'], 'string', 'max' => 2],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'RAION_ID' => 'Raion ID',
            'NAME' => 'Name',
            'LANG' => 'Lang',
        ];
    }
}
