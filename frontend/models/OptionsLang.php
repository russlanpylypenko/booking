<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "yii_options_lang".
 *
 * @property int $ID
 * @property int $OPTION_ID
 * @property string $NAME
 * @property string $LANG
 */
class OptionsLang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_options_lang';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'OPTION_ID' => 'Option ID',
            'NAME' => 'Name',
            'LANG' => 'Lang',
        ];
    }
}
