<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "yii_images".
 *
 * @property int $ID
 * @property string $IMAGE
 * @property string $OBJECT
 * @property int $OBJECT_ID
 * @property int $ORDER
 * @property string $CUSTOM
 * @property string $DESCRIPTION
 * @property string $SLIDER_GROUP_NAME
 */
class Images extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_images';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'IMAGE' => 'Image',
            'OBJECT' => 'Object',
            'OBJECT_ID' => 'Object ID',
            'ORDER' => 'Order',
            'CUSTOM' => 'Custom',
            'DESCRIPTION' => 'Description',
            'SLIDER_GROUP_NAME' => 'Slider Group Name',
        ];
    }
}
