<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "yii_orders".
 *
 * @property int $ID
 * @property int $USER_ID
 * @property int $ZAVEDENIYE_ID
 * @property int $TRANSACTION_ID
 * @property int $MAIL_SENT
 * @property int $SMS_SENT
 * @property int $PAID
 * @property string $CUSTOM
 * @property string $DATE
 * @property string $ORDER_COMMENT
 * @property int $ORDER_DISCOUNT
 * @property int $ORDER_DISCOUNT_TYPE
 * @property string $ORDER_DATE
 * @property string $ORDER_TIME
 * @property int $ORDER_PERSONS
 * @property string $ORDER_KEY
 * @property double $ORDER_AMT
 * @property string $CANCEL_DATE
 * @property string $STATUS
 * @property string $REF_URL
 * @property string $REF_NAME
 * @property int $REF_ID
 * @property string $IP
 * @property double $CLIENT_LAT
 * @property double $CLIENT_LNG
 * @property string $CLIENT_CITY
 * @property string $CLIENT_STREET
 * @property string $CLIENT_REGION
 * @property int $IS_FB
 * @property int $IS_BANQUET
 * @property int $IS_DELETED
 * @property string $CARD_NUMBER
 * @property string $TABLE_NUM
 * @property int $TABLE_ID
 * @property int $MAP_ID
 * @property int $HOSTES_RESERV
 * @property int $MESSAGE_SET
 * @property int $IS_CANCELED
 * @property int $IN_PROCESS
 * @property string $CAME_DATE
 * @property string $CUSTOMER_NAME
 * @property int $PREPAYMENT
 * @property string $CLOSE_DATE
 * @property string $PRECHECK_DATE
 * @property string $TMP_PARAM
 * @property int $IS_INTEGRATED
 * @property int $IS_CONFIRMED
 * @property int $MANAGER_ID
 * @property string $CONFIRM_SOURCE
 * @property int $ZAL_ID
 * @property int $IS_RESTOWEEK
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_orders';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'USER_ID' => 'User ID',
            'ZAVEDENIYE_ID' => 'Zavedeniye ID',
            'TRANSACTION_ID' => 'Transaction ID',
            'MAIL_SENT' => 'Mail Sent',
            'SMS_SENT' => 'Sms Sent',
            'PAID' => 'Paid',
            'CUSTOM' => 'Custom',
            'DATE' => 'Date',
            'ORDER_COMMENT' => 'Order Comment',
            'ORDER_DISCOUNT' => 'Order Discount',
            'ORDER_DISCOUNT_TYPE' => 'Order Discount Type',
            'ORDER_DATE' => 'Order Date',
            'ORDER_TIME' => 'Order Time',
            'ORDER_PERSONS' => 'Order Persons',
            'ORDER_KEY' => 'Order Key',
            'ORDER_AMT' => 'Order Amt',
            'CANCEL_DATE' => 'Cancel Date',
            'STATUS' => 'Status',
            'REF_URL' => 'Ref Url',
            'REF_NAME' => 'Ref Name',
            'REF_ID' => 'Ref ID',
            'IP' => 'Ip',
            'CLIENT_LAT' => 'Client Lat',
            'CLIENT_LNG' => 'Client Lng',
            'CLIENT_CITY' => 'Client City',
            'CLIENT_STREET' => 'Client Street',
            'CLIENT_REGION' => 'Client Region',
            'IS_FB' => 'Is Fb',
            'IS_BANQUET' => 'Is Banquet',
            'IS_DELETED' => 'Is Deleted',
            'CARD_NUMBER' => 'Card Number',
            'TABLE_NUM' => 'Table Num',
            'TABLE_ID' => 'Table ID',
            'MAP_ID' => 'Map ID',
            'HOSTES_RESERV' => 'Hostes Reserv',
            'MESSAGE_SET' => 'Message Set',
            'IS_CANCELED' => 'Is Canceled',
            'IN_PROCESS' => 'In Process',
            'CAME_DATE' => 'Came Date',
            'CUSTOMER_NAME' => 'Customer Name',
            'PREPAYMENT' => 'Prepayment',
            'CLOSE_DATE' => 'Close Date',
            'PRECHECK_DATE' => 'Precheck Date',
            'TMP_PARAM' => 'Tmp Param',
            'IS_INTEGRATED' => 'Is Integrated',
            'IS_CONFIRMED' => 'Is Confirmed',
            'MANAGER_ID' => 'Manager ID',
            'CONFIRM_SOURCE' => 'Confirm Source',
            'ZAL_ID' => 'Zal ID',
            'IS_RESTOWEEK' => 'Is Restoweek',
        ];
    }

}
