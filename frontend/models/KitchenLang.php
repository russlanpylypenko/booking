<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "yii_kitchen_lang".
 *
 * @property int $ID
 * @property int $KITCHEN_ID
 * @property string $NAME
 * @property string $LANG
 */
class KitchenLang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_kitchen_lang';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'KITCHEN_ID' => 'Kitchen ID',
            'NAME' => 'Name',
            'LANG' => 'Lang',
        ];
    }
}
