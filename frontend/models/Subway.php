<?php

namespace frontend\models;

use frontend\components\LanguageHelper;
use Yii;

/**
 * This is the model class for table "yii_subway".
 *
 * @property int $ID
 * @property int $LINE
 * @property int $CITY
 * @property string $SLUG
 */
class Subway extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_subway';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['LINE', 'CITY', 'SLUG'], 'required'],
            [['LINE', 'CITY'], 'integer'],
            [['SLUG'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'LINE' => 'Line',
            'CITY' => 'City',
            'SLUG' => 'Slug',
        ];
    }

    public function getLang()
    {
        return $this->hasOne(SubwayLang::className(), ['SUBWAY_ID' => 'ID'])->where(['LANG' => LanguageHelper::getLangCode()]);
    }
}
