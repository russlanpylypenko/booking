<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "yii_lines_lang".
 *
 * @property int $ID
 * @property int $LINE_ID
 * @property string $NAME
 * @property string $LANG
 */
class LinesLang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_lines_lang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['LINE_ID'], 'required'],
            [['LINE_ID'], 'integer'],
            [['NAME'], 'string'],
            [['LANG'], 'string', 'max' => 2],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'LINE_ID' => 'Line ID',
            'NAME' => 'Name',
            'LANG' => 'Lang',
        ];
    }
}
