<?php

namespace frontend\models;

use frontend\components\LanguageHelper;
use Yii;

/**
 * This is the model class for table "yii_kitchen".
 *
 * @property int $ID
 * @property string $SLUG
 *
 */
class Kitchen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_kitchen';
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'SLUG' => 'Slug',
        ];
    }


    public function getLang()
    {
        return $this->hasOne(KitchenLang::className(), ['KITCHEN_ID' => 'ID'])->where(['LANG' => LanguageHelper::getLangCode()]);;
    }
}
