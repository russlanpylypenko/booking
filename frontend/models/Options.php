<?php

namespace frontend\models;

use frontend\components\LanguageHelper;
use Yii;

/**
 * This is the model class for table "yii_options".
 *
 * @property int $ID
 * @property string $SLUG
 *
 */
class Options extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_options';
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'SLUG' => 'Slug',
        ];
    }


    public function getLang()
    {
        return $this->hasOne(OptionsLang::className(), ['OPTION_ID' => 'ID'])->where(['LANG' => LanguageHelper::getLangCode()]);;
    }
}
