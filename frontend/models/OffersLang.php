<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "yii_offers_lang".
 *
 * @property int $ID
 * @property int $OFFER_ID
 * @property string $NAME
 * @property string $TEXT
 * @property string $SHORT_TEXT
 * @property string $SEO_TITLE
 * @property string $SEO_DESC
 * @property string $SEO_HEADING
 * @property string $SEO_TEXT
 * @property string $LANG
 */
class OffersLang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_offers_lang';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'OFFER_ID' => 'Offer ID',
            'NAME' => 'Name',
            'TEXT' => 'Text',
            'SHORT_TEXT' => 'Short Text',
            'SEO_TITLE' => 'Seo Title',
            'SEO_DESC' => 'Seo Desc',
            'SEO_HEADING' => 'Seo Heading',
            'SEO_TEXT' => 'Seo Text',
            'LANG' => 'Lang',
        ];
    }

    public function getShortText()
    {
        return $this->SHORT_TEXT ? $this->SHORT_TEXT : "---";
    }

    public function getTitle()
    {
        return $this->NAME ? $this->NAME : "---";
    }

    public function getText()
    {
        return $this->TEXT;
    }
}
