<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "yii_page_lang".
 *
 * @property int $ID
 * @property string $LKEY
 * @property string $VAL
 * @property string $LANG
 */
class PageLang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_page_lang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID', 'LKEY', 'VAL', 'LANG'], 'required'],
            [['ID'], 'integer'],
            [['VAL'], 'string'],
            [['LKEY'], 'string', 'max' => 50],
            [['LANG'], 'string', 'max' => 2],
            [['ID', 'LKEY', 'LANG'], 'unique', 'targetAttribute' => ['ID', 'LKEY', 'LANG']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'LKEY' => 'Lkey',
            'VAL' => 'Val',
            'LANG' => 'Lang',
        ];
    }
}
