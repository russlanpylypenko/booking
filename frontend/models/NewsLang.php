<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "yii_news_lang".
 *
 * @property int $ID
 * @property int $NEWS_ID
 * @property string $TITLE
 * @property string $TEXT
 * @property string $SHORT_TEXT
 * @property string $SEO_TITLE
 * @property string $SEO_DESC
 * @property string $SEO_HEADING
 * @property string $SEO_TEXT
 * @property string $LANG
 */
class NewsLang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_news_lang';
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'NEWS_ID' => 'News ID',
            'TITLE' => 'Title',
            'TEXT' => 'Text',
            'SHORT_TEXT' => 'Short Text',
            'SEO_TITLE' => 'Seo Title',
            'SEO_DESC' => 'Seo Desc',
            'SEO_HEADING' => 'Seo Heading',
            'SEO_TEXT' => 'Seo Text',
            'LANG' => 'Lang',
        ];
    }

    public function getShortText()
    {
        return $this->SHORT_TEXT ? $this->SHORT_TEXT : "---";
    }

    public function getTitle()
    {
        return $this->TITLE ? $this->TITLE : "---";
    }

    public function getText()
    {
        return $this->TEXT;
    }

    public function getNew()
    {
        return $this->hasOne(News::className(), ['ID' => "NEWS_ID"]);
    }


}
