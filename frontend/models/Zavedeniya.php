<?php

namespace frontend\models;

use frontend\components\LanguageHelper;
use Yii;

/**
 * This is the model class for table "yii_zavedeniya".
 *
 * @property int $ID
 * @property string $SITE
 * @property string $ADDITION_DATE
 * @property int $SUBWAY
 * @property int $RAION
 * @property int $MARKER_ID
 * @property int $TYPE
 * @property int $VID
 * @property int $VISIBLE
 * @property int $CITY
 * @property int $AVG_BILL
 * @property string $TELEPHONE
 * @property int $DUTY_START
 * @property int $DUTY_END
 * @property string $SLUG
 * @property int $VIEWS
 * @property string $LOGO
 * @property string $VIDEO_DATA
 * @property string $PERSONS_LIMITS
 * @property string $DISCOUNTS_DATA
 * @property string $DISCOUNTS_TYPE
 * @property int $FIXED_MAIN
 * @property int $FIXED_RAND
 * @property int $MAIN_IMAGE_ID
 * @property string $MENU_FILE
 * @property string $MENU_FILE_URL
 * @property string $MENU_FILE_TYPE
 * @property int $AVAILABLE
 * @property int $BANQUET_ON
 * @property int $STATISTICS_ON
 * @property int $ECAMPAIGN_ON
 * @property int $SMS_APPROVE_ON
 * @property int $BOOKING_CABINET_ON
 * @property string $FOURSQUARE_ID
 * @property int $POSITION
 * @property string $CONDITION
 * @property int $BOOKING_CARD_ON
 * @property int $MAX_PEOPLE_COUNT
 * @property int $HIDE_ORDER_COMMENT
 * @property string $NAME_FRAME
 * @property string $FRAME_LANG
 * @property int $HIDE_TITLE
 * @property string $ADRESS_FRAME
 * @property string $ADRESANT_SMS
 * @property string $FRAME_TITLE_ORDER
 * @property int $BANQUET_CHECK
 * @property int $FRAME_COLOR_ISSET
 * @property string $FRAME_COLOR
 * @property int $SMS_SEND
 * @property string $SUCCESS_ORDER_TEXT
 * @property int $TIME_SMS_WAIT
 * @property string $NOAVAILABLE_FRAME_TEXT
 * @property string $SPECIAL_FRAME_TEXT
 * @property int $BANQUET_PRIORITY
 * @property string $GOOGLE_ANALYSTICS_KEY
 * @property int $CATALOG_PRIORITY
 * @property string $TOP_CATALOG_DATE
 * @property string $TOP_ZAVEDENIYA_DATE
 * @property string $TOP_BANQUETS_DATE
 * @property int $TOP_WEEK_POSITION
 * @property string $TOP_WEEK_DATE
 * @property int $FRAME_MAP_ON
 * @property int $IS_EDGING
 * @property string $EDGING_DATE
 * @property string $ORDERED_KITCHEN_IDS
 * @property string $ADR_IN_FRAME
 * @property int $CLOSED_FOREVER
 * @property int $IS_CATALOG
 * @property int $IS_RESTOWEEK
 * @property int $RESTOWEEK_PRICE
 * @property int $RESTOWEEK_TYPE
 *
 * @property YiiKitchenRelations[] $yiiKitchenRelations
 * @property YiiOptionsRelations[] $yiiOptionsRelations
 * @property YiiSpecialDiscount[] $yiiSpecialDiscounts
 * @property YiiUserNote[] $yiiUserNotes
 * @property YiiCities $cITY
 * @property YiiZTypes $tYPE
 * @property YiiZavedeniyaUsersRelations[] $yiiZavedeniyaUsersRelations
 */
class Zavedeniya extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_zavedeniya';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'SITE' => 'Site',
            'ADDITION_DATE' => 'Addition Date',
            'SUBWAY' => 'Subway',
            'RAION' => 'Raion',
            'MARKER_ID' => 'Marker ID',
            'TYPE' => 'Type',
            'VID' => 'Vid',
            'VISIBLE' => 'Visible',
            'CITY' => 'City',
            'AVG_BILL' => 'Avg Bill',
            'TELEPHONE' => 'Telephone',
            'DUTY_START' => 'Duty Start',
            'DUTY_END' => 'Duty End',
            'SLUG' => 'Slug',
            'VIEWS' => 'Views',
            'LOGO' => 'Logo',
            'VIDEO_DATA' => 'Video Data',
            'PERSONS_LIMITS' => 'Persons Limits',
            'DISCOUNTS_DATA' => 'Discounts Data',
            'DISCOUNTS_TYPE' => 'Discounts Type',
            'FIXED_MAIN' => 'Fixed Main',
            'FIXED_RAND' => 'Fixed Rand',
            'MAIN_IMAGE_ID' => 'Main Image ID',
            'MENU_FILE' => 'Menu File',
            'MENU_FILE_URL' => 'Menu File Url',
            'MENU_FILE_TYPE' => 'Menu File Type',
            'AVAILABLE' => 'Available',
            'BANQUET_ON' => 'Banquet On',
            'STATISTICS_ON' => 'Statistics On',
            'ECAMPAIGN_ON' => 'Ecampaign On',
            'SMS_APPROVE_ON' => 'Sms Approve On',
            'BOOKING_CABINET_ON' => 'Booking Cabinet On',
            'FOURSQUARE_ID' => 'Foursquare ID',
            'POSITION' => 'Position',
            'CONDITION' => 'Condition',
            'BOOKING_CARD_ON' => 'Booking Card On',
            'MAX_PEOPLE_COUNT' => 'Max People Count',
            'HIDE_ORDER_COMMENT' => 'Hide Order Comment',
            'NAME_FRAME' => 'Name Frame',
            'FRAME_LANG' => 'Frame Lang',
            'HIDE_TITLE' => 'Hide Title',
            'ADRESS_FRAME' => 'Adress Frame',
            'ADRESANT_SMS' => 'Adresant Sms',
            'FRAME_TITLE_ORDER' => 'Frame Title Order',
            'BANQUET_CHECK' => 'Banquet Check',
            'FRAME_COLOR_ISSET' => 'Frame Color Isset',
            'FRAME_COLOR' => 'Frame Color',
            'SMS_SEND' => 'Sms Send',
            'SUCCESS_ORDER_TEXT' => 'Success Order Text',
            'TIME_SMS_WAIT' => 'Time Sms Wait',
            'NOAVAILABLE_FRAME_TEXT' => 'Noavailable Frame Text',
            'SPECIAL_FRAME_TEXT' => 'Special Frame Text',
            'BANQUET_PRIORITY' => 'Banquet Priority',
            'GOOGLE_ANALYSTICS_KEY' => 'Google Analystics Key',
            'CATALOG_PRIORITY' => 'Catalog Priority',
            'TOP_CATALOG_DATE' => 'Top Catalog Date',
            'TOP_ZAVEDENIYA_DATE' => 'Top Zavedeniya Date',
            'TOP_BANQUETS_DATE' => 'Top Banquets Date',
            'TOP_WEEK_POSITION' => 'Top Week Position',
            'TOP_WEEK_DATE' => 'Top Week Date',
            'FRAME_MAP_ON' => 'Frame Map On',
            'IS_EDGING' => 'Is Edging',
            'EDGING_DATE' => 'Edging Date',
            'ORDERED_KITCHEN_IDS' => 'Ordered Kitchen Ids',
            'ADR_IN_FRAME' => 'Adr In Frame',
            'CLOSED_FOREVER' => 'Closed Forever',
            'IS_CATALOG' => 'Is Catalog',
            'IS_RESTOWEEK' => 'Is Restoweek',
            'RESTOWEEK_PRICE' => 'Restoweek Price',
            'RESTOWEEK_TYPE' => 'Restoweek Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYiiKitchenRelations()
    {
        return $this->hasMany(YiiKitchenRelations::className(), ['FROM_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYiiOptionsRelations()
    {
        return $this->hasMany(YiiOptionsRelations::className(), ['FROM_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYiiSpecialDiscounts()
    {
        return $this->hasMany(YiiSpecialDiscount::className(), ['ZAVEDENIYE_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYiiUserNotes()
    {
        return $this->hasMany(YiiUserNote::className(), ['ZAVEDENIE_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCITY()
    {
        return $this->hasOne(YiiCities::className(), ['ID' => 'CITY']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTYPE()
    {
        return $this->hasOne(YiiZTypes::className(), ['ID' => 'TYPE']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYiiZavedeniyaUsersRelations()
    {
        return $this->hasMany(YiiZavedeniyaUsersRelations::className(), ['TO_ID' => 'ID']);
    }

    public function getLang()
    {
        return $this->hasOne(YiiZavedeniyaLang::className(), ['ZAVEDENIYE_ID' => 'ID'])->where(['LANG' => LanguageHelper::getLangCode()]);
    }

    public function getImages()
    {
        return $this->hasOne(Images::className(), ['OBJECT_ID' => 'ID'])->where(['OBJECT' => 'Zavedeniya']);
    }

    public function getSubway()
    {
        return $this->hasOne(Subway::className(), ['ID' => 'SUBWAY']);
    }

    public function getName()
    {
        return $this->lang->NAME;
    }

    public function getAddress()
    {
        return $this->lang->ADDRESS;
    }

    public function getViews()
    {
        return $this->VIEWS;
    }

    public function getMainImageFile()
    {
        $Image = $this->getImages()->andWhere(['ID' => $this->MAIN_IMAGE_ID])->one();

        $src = $Image ? '/uploads/img/zavedeniya/' . $Image->IMAGE : 'https://picsum.photos/175';
        return $src;
    }

    public function getLogoFile()
    {
        return $this->LOGO ? '/uploads/img/zavedeniya/' . $this->LOGO : 'https://picsum.photos/175';
    }
}
