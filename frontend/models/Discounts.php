<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "yii_discounts".
 *
 * @property int $ID
 * @property int $ZAVEDENIYE_ID
 * @property int $ON
 * @property string $DATE
 * @property int $DAY_OF_WEEK
 * @property string $TIME_FROM
 * @property string $TIME_TO
 * @property int $VALUE
 */
class Discounts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_discounts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ZAVEDENIYE_ID', 'ON', 'DATE', 'DAY_OF_WEEK', 'TIME_FROM', 'TIME_TO', 'VALUE'], 'required'],
            [['ZAVEDENIYE_ID', 'ON', 'DAY_OF_WEEK', 'VALUE'], 'integer'],
            [['DATE', 'TIME_FROM', 'TIME_TO'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ZAVEDENIYE_ID' => 'Zavedeniye ID',
            'ON' => 'On',
            'DATE' => 'Date',
            'DAY_OF_WEEK' => 'Day Of Week',
            'TIME_FROM' => 'Time From',
            'TIME_TO' => 'Time To',
            'VALUE' => 'Value',
        ];
    }
}
