<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "yii_zz_types_lang".
 *
 * @property int $ID
 * @property int $ZZTYPE_ID
 * @property string $NAME
 * @property string $LANG
 */
class ZzTypesLang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yii_zz_types_lang';
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ZZTYPE_ID' => 'Zztype ID',
            'NAME' => 'Name',
            'LANG' => 'Lang',
        ];
    }
}
