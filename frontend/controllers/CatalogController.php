<?php

namespace frontend\controllers;

use frontend\models\Kitchen;
use frontend\models\Lines;
use frontend\models\Options;
use frontend\models\Raion;
use frontend\models\Subway;
use frontend\models\Zavedeniya;
use frontend\models\ZzTypes;
use Yii;
use yii\redis\Connection;
use yii\web\Response;

class CatalogController extends \yii\web\Controller
{
    public function actionIndex($page = 1)
    {
        return $this->render('index', [
            'page' => $page,
        ]);
    }

    public function actionGetFilter()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /* @var $redis Connection */
        $redis = Yii::$app->redis;

        $filter = [];

        $options = Options::find()->with([
            'lang' => function ($query) {
                $query->select('NAME, OPTION_ID');
            }
        ])->select(["ID"])->asArray()->all();

        foreach ($options as $option) {
            if ($option['lang'] == null || $option['lang']['NAME'] == null) {
                continue;
            }
            $filter['options'][] = [
                'ID' => $option['ID'],
                'NAME' => $option['lang']['NAME'],
                'type' => 'option',
            ];
        }

        $kitchens = Kitchen::find()->with([
            'lang' => function ($query) {
                $query->select('NAME, KITCHEN_ID');
            }
        ])->select(["ID"])->asArray()->all();

        foreach ($kitchens as $item) {
            if ($item['lang'] == null || $item['lang']['NAME'] == null) {
                continue;
            }
            $filter['kitchens'][] = [
                'ID' => $item['ID'],
                'NAME' => $item['lang']['NAME'],
                'type' => 'kitchen',
            ];
        }

        $types = ZzTypes::find()->with([
            'lang' => function ($query) {
                $query->select('NAME, ZZTYPE_ID');
            }
        ])->select(["ID"])->asArray()->all();

        foreach ($types as $item) {
            if ($item['lang'] == null || $item['lang']['NAME'] == null) {
                continue;
            }
            $filter['types'][] = [
                'ID' => $item['ID'],
                'NAME' => $item['lang']['NAME'],
                'type' => 'type',
            ];
        }


        $raions = Raion::find()->where(["CITY" => Yii::$app->params['city']])->with([
            'lang' => function ($query) {
                $query->select('NAME, RAION_ID');
            }
        ])->select(["ID"])->asArray()->all();

        foreach ($raions as $item) {
            if ($item['lang'] == null || $item['lang']['NAME'] == null) {
                continue;
            }
            $filter['raions'][] = [
                'ID' => $item['ID'],
                'NAME' => $item['lang']['NAME'],
                'type' => 'raion',
            ];
        }

        $lines = Lines::find()->where(["CITY" => Yii::$app->params['city']])->with([
            'lang' => function ($query) {
                $query->select('NAME, LINE_ID');
            }
        ])->select(["ID"])->asArray()->all();

        foreach ($lines as $item) {
            if ($item['lang'] == null || $item['lang']['NAME'] == null) {
                continue;
            }
            $filter['lines'][] = [
                'ID' => $item['ID'],
                'NAME' => explode(" ", $item['lang']['NAME'])[0],
                'type' => 'line',
            ];
        }

        $subways = Subway::find()
            ->where(["CITY" => Yii::$app->params['city']])
            ->with('lang')
            ->asArray()
            ->all();

        foreach ($subways as $item) {
            if ($item['lang'] == null || $item['lang']['NAME'] == null) {
                continue;
            }
            $filter['subways'][] = [
                'ID' => $item['ID'],
                'NAME' => $item['lang']['NAME'],
                'LINE' => $item['LINE'],
                'type' => 'subway',
            ];
        }

        return [
            'success' => true,
            'filter' => $filter
        ];
    }

    public function actionGetFilterCounts()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->isAjax) {
            /* @var $redis Connection */
            $redis = Yii::$app->redis;

            $filter = Yii::$app->request->post();

            $city = Yii::$app->params['city'];


            if (!empty($filter)) {
                $restaurant_ids = [];
                $flag = true;
                if (isset($filter['options'])) {
                    foreach ($filter['options'] as $option) {
                        if (empty($restaurant_ids) && $flag) {
                            $restaurant_ids = array_merge($restaurant_ids, $redis->smembers("city:{$city}:option:{$option}:restaurant"));
                            $flag = false;
                        } else {
                            $restaurant_ids = array_intersect($restaurant_ids, $redis->smembers("city:{$city}:option:{$option}:restaurant"));
                        }

                    }
                }

                if (isset($filter['kitchens'])) {
                    foreach ($filter['kitchens'] as $item) {
                        if (empty($restaurant_ids) && $flag) {
                            $restaurant_ids = array_merge($restaurant_ids, $redis->smembers("city:{$city}:kitchen:{$item}:restaurant"));
                            $flag = false;
                        } else {
                            $restaurant_ids = array_intersect($restaurant_ids, $redis->smembers("city:{$city}:kitchen:{$item}:restaurant"));
                        }

                    }
                }

                if (isset($filter['types'])) {
                    foreach ($filter['types'] as $item) {
                        if (empty($restaurant_ids) && $flag) {
                            $restaurant_ids = array_merge($restaurant_ids, $redis->smembers("city:{$city}:vid:{$item}:restaurant"));
                            $flag = false;
                        } else {
                            $restaurant_ids = array_intersect($restaurant_ids, $redis->smembers("city:{$city}:vid:{$item}:restaurant"));
                        }

                    }
                }

                if (isset($filter['subways'])) {
                    foreach ($filter['subways'] as $item) {
                        if (empty($restaurant_ids) && $flag) {
                            $restaurant_ids = array_merge($restaurant_ids, $redis->smembers("city:{$city}:subway:{$item}:restaurant"));
                            $flag = false;
                        } else {
                            $restaurant_ids = array_intersect($restaurant_ids, $redis->smembers("city:{$city}:subway:{$item}:restaurant"));
                        }

                    }
                }

                if (isset($filter['raions'])) {
                    foreach ($filter['raions'] as $item) {
                        if (empty($restaurant_ids) && $flag) {
                            $restaurant_ids = array_merge($restaurant_ids, $redis->smembers("city:{$city}:raion:{$item}:restaurant"));
                            $flag = false;
                        } else {
                            $restaurant_ids = array_intersect($restaurant_ids, $redis->smembers("city:{$city}:raion:{$item}:restaurant"));
                        }

                    }
                }


                $restaurant_ids = array_unique($restaurant_ids);
            }


            $options = Options::find()->select(["ID"])->asArray()->all();
            foreach ($options as &$option) {
                $r_ids = $redis->smembers("city:{$city}:option:{$option['ID']}:restaurant");
                $count = !isset($restaurant_ids) ? count($r_ids) : count(array_intersect($r_ids, $restaurant_ids));
                $option['count'] = $count;
            }

            $kitchens = Kitchen::find()->select(["ID"])->asArray()->all();
            foreach ($kitchens as &$kitchen) {
                $r_ids = $redis->smembers("city:{$city}:kitchen:{$kitchen['ID']}:restaurant");
                $count = !isset($restaurant_ids) ? count($r_ids) : count(array_intersect($r_ids, $restaurant_ids));
                $kitchen['count'] = $count;
            }

            $types = ZzTypes::find()->select(["ID"])->asArray()->all();
            foreach ($types as &$type) {
                $r_ids = $redis->smembers("city:{$city}:vid:{$type['ID']}:restaurant");
                $count = !isset($restaurant_ids) ? count($r_ids) : count(array_intersect($r_ids, $restaurant_ids));
                $type['count'] = $count;
            }

            $subways = Subway::find()->select(["ID"])->asArray()->all();
            foreach ($subways as &$subway) {
                $r_ids = $redis->smembers("city:{$city}:subway:{$subway['ID']}:restaurant");
                $count = !isset($restaurant_ids) ? count($r_ids) : count(array_intersect($r_ids, $restaurant_ids));
                $subway['count'] = $count;
            }

            $raions = Raion::find()->select(["ID"])->asArray()->all();
            foreach ($raions as &$raion) {
                $r_ids = $redis->smembers("city:{$city}:raion:{$raion['ID']}:restaurant");
                $count = !isset($restaurant_ids) ? count($r_ids) : count(array_intersect($r_ids, $restaurant_ids));
                $raion['count'] = $count;
            }


        }

        return [
            'success' => true,
            'counts' => [
                'options' => $options,
                'kitchens' => $kitchens,
                'types' => $types,
                'subways' => $subways,
                'raions' => $raions,
            ],
        ];
    }

    public function actionGetRestaurants()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->isAjax) {
            $filter = Yii::$app->request->post();

            $city = Yii::$app->params['city'];

            $restaurant_ids = [];

            $query = Zavedeniya::find()->select('ID, VISIBLE, AVG_BILL, VIEWS, MAIN_IMAGE_ID')
                ->where(['VISIBLE' => 1])->andWhere(['CITY' => Yii::$app->params['city']])->with([
                    'lang' => function ($query) {
                        $query->select('NAME, ADDRESS, ZAVEDENIYE_ID, LANG');
                    },
                ])->orderBy(['VIEWS' => SORT_DESC]);

            if (!empty($filter)) {
                /* @var $redis Connection */
                $redis = Yii::$app->redis;
                $flag = true;

                if (isset($filter['options'])) {
                    foreach ($filter['options'] as $option) {
                        if (empty($restaurant_ids) && $flag) {
                            $restaurant_ids = array_merge($restaurant_ids, $redis->smembers("city:{$city}:option:{$option}:restaurant"));
                            $flag = false;
                        } else {
                            $restaurant_ids = array_intersect($restaurant_ids, $redis->smembers("city:{$city}:option:{$option}:restaurant"));
                        }
                    }
                }

                if (isset($filter['kitchens'])) {
                    foreach ($filter['kitchens'] as $item) {
                        if (empty($restaurant_ids) && $flag) {
                            $restaurant_ids = array_merge($restaurant_ids, $redis->smembers("city:{$city}:kitchen:{$item}:restaurant"));
                            $flag = false;
                        } else {
                            $restaurant_ids = array_intersect($restaurant_ids, $redis->smembers("city:{$city}:kitchen:{$item}:restaurant"));
                        }
                    }
                }


                if (isset($filter['types'])) {
                    foreach ($filter['types'] as $item) {
                        if (empty($restaurant_ids) && $flag) {
                            $restaurant_ids = array_merge($restaurant_ids, $redis->smembers("city:{$city}:vid:{$item}:restaurant"));
                            $flag = false;
                        } else {
                            $restaurant_ids = array_intersect($restaurant_ids, $redis->smembers("city:{$city}:vid:{$item}:restaurant"));
                        }
                    }
                }

                if (isset($filter['subways'])) {
                    foreach ($filter['subways'] as $item) {
                        if (empty($restaurant_ids) && $flag) {
                            $restaurant_ids = array_merge($restaurant_ids, $redis->smembers("city:{$city}:subway:{$item}:restaurant"));
                            $flag = false;
                        } else {
                            $restaurant_ids = array_intersect($restaurant_ids, $redis->smembers("city:{$city}:subway:{$item}:restaurant"));
                        }
                    }
                }

                if (isset($filter['raions'])) {
                    foreach ($filter['raions'] as $item) {
                        if (empty($restaurant_ids) && $flag) {
                            $restaurant_ids = array_merge($restaurant_ids, $redis->smembers("city:{$city}:raion:{$item}:restaurant"));
                            $flag = false;
                        } else {
                            $restaurant_ids = array_intersect($restaurant_ids, $redis->smembers("city:{$city}:raion:{$item}:restaurant"));
                        }
                    }
                }

                $restaurant_ids = array_unique($restaurant_ids);

                $query->andWhere(['in', "ID", $restaurant_ids]);
            }


            $limit = \Yii::$app->params['restaurantsPerPage'];
            $restaurants = $query
                ->limit($limit)
                ->offset(0)
                ->andWhere(['CITY' => Yii::$app->params['city']])
                ->andWhere(['VISIBLE' => 1])
                ->orderBy(['VIEWS' => SORT_DESC])
                ->all();


            foreach ($restaurants as &$restaurant) {
                $img = $restaurant->getMainImageFile();
                $lang = $restaurant->lang;
                $restaurant = $restaurant->toArray();
                $restaurant['img'] = $img;
                $restaurant['lang'] = $lang;
            }


            return [
                'success' => true,
                'filter' => $filter,
                'restaurants' => $restaurants,
                'restaurants_ids' => $restaurant_ids,
            ];
        }

    }


    public function actionView()
    {
        return $this->render('view');
    }

    public function actionUpdateFilter()
    {
        /* @var $redis Connection */
        $redis = Yii::$app->redis;

        $redis->flushall();

        $this->option();
        $this->kitchen();
        $this->vid();
        $this->subway();
        $this->raion();

        echo "END";

    }

    private function option()
    {
        echo "<pre>";
        $resultList = Yii::$app->db->createCommand('SELECT * FROM yii_options_relations')->queryAll();

        /* @var $redis Connection */
        $redis = Yii::$app->redis;

        $city = Yii::$app->params['city'];
        foreach ($resultList as $result) {
            $restoran = Zavedeniya::find()->where(['VISIBLE' => 1])->andWhere(['ID' => $result['FROM_ID']])->one();
            if ($restoran && $restoran->CITY == $city) {
                $redis->sadd("city:{$city}:option:{$result['TO_ID']}:restaurant", $result['FROM_ID']);
            }

        }
    }


    private function kitchen()
    {
        echo "<pre>";
        $resultList = Yii::$app->db->createCommand('SELECT * FROM yii_kitchen_relations')->queryAll();

        /* @var $redis Connection */
        $redis = Yii::$app->redis;

        $city = Yii::$app->params['city'];
        foreach ($resultList as $result) {
            $restoran = Zavedeniya::find()->where(['VISIBLE' => 1])->andWhere(['ID' => $result['FROM_ID']])->one();
            if ($restoran && $restoran->CITY == $city) {
                $redis->sadd("city:{$city}:kitchen:{$result['TO_ID']}:restaurant", $result['FROM_ID']);
            }

        }
    }

    private function vid()
    {
        echo "<pre>";
        $city = Yii::$app->params['city'];
        $resultList = Yii::$app->db->createCommand('SELECT * FROM yii_zavedeniya WHERE VISIBLE = 1 AND CITY = ' . $city)->queryAll();

        /* @var $redis Connection */
        $redis = Yii::$app->redis;


        foreach ($resultList as $result) {
            $redis->sadd("city:{$city}:vid:{$result['VID']}:restaurant", $result['ID']);
        }
    }

    private function subway()
    {
        echo "<pre>";
        $city = Yii::$app->params['city'];
        $resultList = Yii::$app->db->createCommand('SELECT * FROM yii_zavedeniya WHERE VISIBLE = 1 AND CITY = ' . $city)->queryAll();

        /* @var $redis Connection */
        $redis = Yii::$app->redis;


        foreach ($resultList as $result) {
            $redis->sadd("city:{$city}:subway:{$result['SUBWAY']}:restaurant", $result['ID']);
        }
    }

    private function raion()
    {
        echo "<pre>";
        $city = Yii::$app->params['city'];
        $resultList = Yii::$app->db->createCommand('SELECT * FROM yii_zavedeniya WHERE VISIBLE = 1 AND CITY = ' . $city)->queryAll();

        /* @var $redis Connection */
        $redis = Yii::$app->redis;



        foreach ($resultList as $result) {
            $redis->sadd("city:{$city}:raion:{$result['RAION']}:restaurant", $result['ID']);
        }
    }


}
