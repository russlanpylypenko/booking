<?php

namespace frontend\controllers;

use frontend\models\Discounts;
use frontend\models\News;
use frontend\models\Offers;
use frontend\models\Page;
use frontend\models\Topics;
use frontend\models\Zavedeniya;
use frontend\models\forms\SearchForm;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\Cookie;

/**
 * Site controller
 */
class SiteController extends Controller
{
    const PAGE_PATH = 'main';
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {

        //новые заведения
        $query = Zavedeniya::find()->select('ID, VISIBLE, AVG_BILL, VIEWS, MAIN_IMAGE_ID, ADDITION_DATE')->with([
            'lang' => function ($query) {
                $query->select('NAME, ADDRESS, ZAVEDENIYE_ID');
            },
        ])->orderBy(['ADDITION_DATE' => SORT_DESC]);

        $limit = \Yii::$app->params['newRestaurantsLimit'];
        $newRestaurants = $query
            ->limit($limit)
            ->all();


        //Популярные заведения
        $query = Zavedeniya::find()->select('ID, VISIBLE, AVG_BILL, VIEWS, MAIN_IMAGE_ID, POSITION')->with([
            'lang' => function ($query) {
                $query->select('NAME, ADDRESS, ZAVEDENIYE_ID');
            },
        ])->orderBy(['POSITION' => SORT_ASC])->where("POSITION IS NOT NULL");

        $limit = \Yii::$app->params['popularRestaurantsLimit'];
        $popularRestaurants = $query
            ->limit($limit)
            ->all();


        //Заведения со скидкой 20 - 50%

        $restaurantsWithDiscounts = $this->getRestaurantWithDiscount();
        $specialOffers = $this->getSpecOffers();
        $restaurantsOfWeek = $this->getRestaurantsOfWeek();
        $firstRestaurantOfWeek = null;
        if ($restaurantsOfWeek) $firstRestaurantOfWeek = array_shift($restaurantsOfWeek);
        $restonRecomendedTopicParts = $this->getRestonRecomendedTopics();

        $news = $this->getNews();
        $topics = $this->getReviews();

        $text = $this->getText();

        return $this->render('index', [
            'newRestaurants' => $newRestaurants,
            'popularRestaurants' => $popularRestaurants,
            'restaurantsWithDiscounts' => $restaurantsWithDiscounts,
            'specialOffers' => $specialOffers,
            'restaurantsOfWeek' => $restaurantsOfWeek,
            'firstRestaurantsOfWeek' => $firstRestaurantOfWeek,
            'restonRecomendedTopicParts' => $restonRecomendedTopicParts,
            'news' => $news,
            'topics' => $topics,
            'text' => $text,
        ]);
    }

    private function getText()
    {
        $text = Page::find()
            ->with('lang')
            ->where(['PATH' => self::PAGE_PATH])
            ->one();

        return $text;
    }

    private function getNews()
    {
        $news = News::find()
            ->with('lang')
            ->where("DATE_END > '" . date('Y-m-d') . "'")
            ->orWhere("DATE_END = '0000-00-00'")
            ->andWhere(['CITY' => Yii::$app->params['city']])
            ->andWhere("DATE < '" . date('Y-m-d H:i') . "'")
            ->limit(Yii::$app->params['newsAndReviewsMainPageLimit'])
            ->orderBy([
                'DATE' => SORT_DESC,
                'ID' => SORT_DESC
            ])->all();

        return $news;
    }

    private function getReviews()
    {

        $topics = Topics::find()
            ->with('lang')
            ->where(['CITY' => Yii::$app->params['city']])
            ->andWhere("FIXED_MAIN != 1")
            ->andWhere("DATE < '" . date('Y-m-d H:i') . "'")
            ->limit(Yii::$app->params['newsAndReviewsMainPageLimit'])
            ->orderBy([
                'DATE' => SORT_DESC,
                'ID' => SORT_DESC
            ])->all();

        return $topics;
    }

    private function getRestonRecomendedTopics()
    {
        $restonRecomended = Topics::find()->select("ID, DATE, SLUG, IMAGE")->with([
            'lang' => function ($query) {
                $query->select('TITLE, SHORT_TEXT,TOPIC_ID');
            },
        ])->where("DATE < '" . date('Y-m-d H:i') . "'")
            ->andWhere(['CITY' => Yii::$app->params['city']])
            ->andWhere(['FIXED_MAIN' => 1])
            ->orderBy(['RESTON_RECOMENDED_ORDER' => SORT_ASC])
            ->limit(Yii::$app->params['restonRecomendedTopics'])
            ->all();

        $result = array_chunk($restonRecomended, 2);

        return $result;
    }

    private function getRestaurantsOfWeek()
    {
        $restaurantsOfWeek = Zavedeniya::find()
            ->select('ID, VISIBLE, AVG_BILL, VIEWS, MAIN_IMAGE_ID, SLUG, LOGO')->with([
                'lang' => function ($query) {
                    $query->select('NAME, ADDRESS, ZAVEDENIYE_ID');
                },
            ])->limit(Yii::$app->params['restaurantsOfWeekLimit'])
            ->where('TOP_WEEK_POSITION IS NOT NULL')
            ->andWhere(['AVAILABLE' => 1])
            ->andWhere(['VISIBLE' => 1])
            ->orderBy(['TOP_WEEK_POSITION' => SORT_ASC])
            ->all();

        return $restaurantsOfWeek;
    }

    private function getSpecOffers()
    {

        $specOffers = Offers::find()->with(['lang', 'restaurant'])
            ->where("DATE_END > '" . date('Y-m-d') . "'")
            ->orWhere('DATE_END = "0000-00-00"')
            ->andWhere("DATE < '" . date('Y-m-d H:i') . "'")
            ->limit(Yii::$app->params['specOffersMainPageLimit'])
            ->orderBy(['ID' => SORT_DESC])
            ->all();

        return $specOffers;
    }

    private function getRestaurantWithDiscount()
    {
        $limit = \Yii::$app->params['newRestaurantsLimit'];
        $discounts = Discounts::find()
            ->select('ZAVEDENIYE_ID, DATE')
            ->distinct(true)
            ->where('VALUE > 19')
            ->andWhere('VALUE < 51')
            ->orderBy(['DATE' => SORT_DESC])
            ->limit(30)->asArray()->all();

        foreach ($discounts as $D) {
            $ZavedeniaViaDiscount[] = $D['ZAVEDENIYE_ID'];
        }

        $restaurantsWithDiscounts = Zavedeniya::find()
            ->select('ID, VISIBLE, AVG_BILL, VIEWS, MAIN_IMAGE_ID')->with([
                'lang' => function ($query) {
                    $query->select('NAME, ADDRESS, ZAVEDENIYE_ID');
                },
            ])->limit($limit)
            ->where(['ID' => $ZavedeniaViaDiscount])
            ->andWhere(['AVAILABLE' => 1])
            ->andWhere(['VISIBLE' => 1])
            ->all();

        return $restaurantsWithDiscounts;
    }


    public function actionLanguage()
    {
        $language = Yii::$app->request->post('language');
        Yii::$app->language = $language;

        //  var_dump($language); die();

        $languageCookie = new Cookie([
            'name' => 'language',
            'value' => $language,
            'expire' => time() + 60 * 60 * 24 * 30, // 30 days
        ]);
        Yii::$app->response->cookies->add($languageCookie);

        return $this->goBack();
    }

    public function actionSetCity($id)
    {
        Yii::$app->params['city'] = (int)$id;

        $cityCookie = new Cookie([
            'name' => 'city',
            'value' => (int)$id,
            'expire' => time() + 60 * 60 * 24 * 30, // 30 days
        ]);
        Yii::$app->response->cookies->add($cityCookie);

        return $this->goBack();
    }


}
