<?php

namespace frontend\controllers;

use frontend\models\forms\OrderForm;
use Yii;
use yii\web\Response;
use yii\web\Controller;

class OrderController extends Controller
{
    /**
     * @return array
     */
    public function actionGetAvailableTimes()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        return [
            'times' => ['15:00', '15:30']
        ];
    }

    public function actionSave()
    {


        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new OrderForm();

        if (Yii::$app->request->isAjax){

            if ($model->save()) {
                return [
                    'success' => true,
                    'data' => $model,
                ];
            }

            return [
                'success' => false,
                'message' => $model->getErrors(),
            ];

        }

    }

}