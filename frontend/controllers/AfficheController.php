<?php

namespace frontend\controllers;

use frontend\models\News;
use frontend\models\Offers;

class AfficheController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $offers = Offers::find()->with('lang')->where(['DATE' => date("Y-m-d", strtotime('2019-02-14'))])->asArray()->all();
        $news = News::find()->with('lang')->where(['DATE' => date("Y-m-d", strtotime('2019-02-15'))])->asArray()->all();

        foreach ($offers as &$offer){$offer['type'] = 'offer';}
        foreach ($news as &$new){$new['type'] = 'news';}

        $afficheList = array_merge($news, $offers);
        $this->shuffle_assoc($afficheList);

        return $this->render('index', [
            'afficheList' => $afficheList,
        ]);
    }

    private function shuffle_assoc(&$array) {
        $keys = array_keys($array);

        shuffle($keys);

        foreach($keys as $key) {
            $new[$key] = $array[$key];
        }

        $array = $new;

        return true;
    }
}
