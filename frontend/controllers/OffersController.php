<?php

namespace frontend\controllers;

use frontend\models\Offers;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;

class OffersController extends \yii\web\Controller
{
    public function actionIndex($page = 1)
    {
        $query = Offers::find()->with('lang')->orderBy(['DATE' => SORT_DESC]);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);

        $limit = \Yii::$app->params['articlesPerPage'];
        $offset = ($page-1) * $limit;
        $offerList = $query
            ->limit($limit)
            ->offset($offset)
            ->all();

        return $this->render('index', [
            'offerList' => $offerList,
            'pages' => $pages
        ]);
    }

    public function actionView($slug)
    {
        $article = $this->findArticle($slug);
        return $this->render('view', [
            'article' => $article
        ]);
    }

    public function findArticle($slug)
    {
        if ($article = Offers::find()->with('lang')->where(['SLUG' => $slug])->one()) {
            return $article;
        }
        throw new NotFoundHttpException();
    }


}
