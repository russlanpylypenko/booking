<?php

namespace frontend\controllers;

use frontend\models\News;
use frontend\models\Topics;
use Yii;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;

class TopicsController extends \yii\web\Controller
{
    public function actionIndex($page = 1)
    {
        $query = Topics::find()->with('lang')->orderBy(['DATE' => SORT_DESC]);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);

        $limit = \Yii::$app->params['articlesPerPage'];
        $offset = ($page-1) * $limit;
        $topicList = $query
            ->limit($limit)
            ->where(['CITY' => Yii::$app->params['city']])
            ->andWhere("DATE < '" . date('Y-m-d H:i') . "'")
            ->offset($offset)
            ->all();

        $newsList = News::find()->with('lang')->limit(10)->orderBy(['DATE' => SORT_DESC])->asArray()->all();

        return $this->render('index', [
            'topicList' => $topicList,
            'pages' => $pages,
            'newsList' => $newsList,
        ]);
    }

    public function actionView($slug)
    {
        $article = $this->findArticle($slug);
        return $this->render('view', [
            'article' => $article
        ]);
    }

    public function findArticle($slug)
    {
        if ($article = Topics::find()->with('lang')->where(['SLUG' => $slug])->one()) {
            return $article;
        }
        throw new NotFoundHttpException();
    }

}
