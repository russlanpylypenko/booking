<?php

namespace frontend\controllers;

use Yii;
use frontend\models\forms\SearchForm;
use yii\web\Controller;

class SearchController extends Controller
{
    public function actionIndex()
    {
        $searchForm = new SearchForm();
        $results = [];

        if(Yii::$app->request->isGet){
            $searchForm->q = Yii::$app->request->get('q');
            $results = $searchForm->search();
        }

        return $this->render('index', [
            'searchForm' => $searchForm,
            'results' => $results
        ]);
    }
}