<?php

namespace frontend\controllers;

use frontend\models\News;
use frontend\models\NewsLang;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;

class NewsController extends \yii\web\Controller
{
    public function actionIndex($page = 1)
    {
        $query = News::find()->with('lang')->orderBy(['DATE' => SORT_DESC])
            ->where("DATE < '" . date('Y-m-d H:i') . "'")
            ->orWhere("DATE_END > '" . date('Y-m-d H:i') . "'")
            ->andWhere("DATE_END = '0000-00-00'");
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);

        $limit = \Yii::$app->params['articlesPerPage'];
        $offset = ($page - 1) * $limit;
        $newsList = $query
            ->limit($limit)
            ->offset($offset)
            ->all();


        return $this->render('index', [
            'newsList' => $newsList,
            'pages' => $pages
        ]);
    }

    public function actionView($slug)
    {
        $article = $this->findArticle($slug);
        return $this->render('view', [
            'article' => $article
        ]);
    }


    public function findArticle($slug)
    {
        if ($article = News::find()->with('lang')->where(['SLUG' => $slug])->one()) {
            return $article;
        }
        throw new NotFoundHttpException();
    }

}
