<?php

namespace frontend\modules\user\controllers;

use frontend\models\forms\SignUpForm;
use Yii;
use frontend\models\forms\LoginForm;
use yii\web\Response;
use yii\web\Controller;

class UserController extends Controller
{
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        $loginForm = new LoginForm();

        if (Yii::$app->request->isAjax){
            $loginForm->email = Yii::$app->request->post('email');
            $loginForm->password = Yii::$app->request->post('password');

            if (!$loginForm->login()) {
                return [
                    'success' => false,
                    'message' => $loginForm->getFirstError('password'),
                ];
            }

            return [
                'success' => true,
                'message' => 'Вы успешно авторизованы!',
            ];

        }

    }


    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    public function actionSignUp()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new SignUpForm();

        if (Yii::$app->request->isAjax){
            $model->attributes = Yii::$app->request->post();

            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {

                    return [
                        'success' => true,
                        'message' => 'Вы успешно зарегистрировались!',
                    ];
                }
            }

            return [
                'success' => false,
                'message' => $model->getErrors(),
            ];

        }
    }
}