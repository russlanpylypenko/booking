<?php

namespace frontend\modules\user\controllers;

use Yii;
use yii\web\Controller;

class ProfileController extends Controller
{
    public function actionView()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        return $this->render('view');
    }

}