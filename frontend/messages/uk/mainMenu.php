<?php

return [
    'catalog' => 'Заклади',
    'banquets' => 'Банкетні зали',
    'news' => 'Новини',
    'topics' => 'Огляди',
    'affiche' => 'Афіша',
];