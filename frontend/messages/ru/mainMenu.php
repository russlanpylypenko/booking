<?php

return [
    'catalog' => 'Заведения',
    'banquets' => 'Банкетные залы',
    'news' => 'Новости',
    'topics' => 'Обзоры',
    'affiche' => 'Афиша',
];