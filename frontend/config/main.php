<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'language' => 'ru',
    'bootstrap' => [
        'log',
        [
            'class' => 'frontend\components\CityBootstrap',
        ],
    ],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'user' => [
            'class' => 'frontend\modules\user\Module',
        ],
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'frontend\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource'
                ],
            ],
        ],
        'languageDropdown' => [
            'class' => 'frontend\components\LanguageDropdown'
        ],
        'city' => [
            'class' => 'frontend\components\CityDropdown'
        ],
        'urlManager' => [
            'class' => 'codemix\localeurls\UrlManager',
            'languages' => ['ukr'=>'uk', 'ru'],
            'enableDefaultLanguageUrlCode' => false,
            'enableLanguagePersistence' => true,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => 'site/index',
                'news/' => 'news/index',
                'news/<slug:[A-Za-z0-9\-]+>' => 'news/view',
                'topics/' => 'topics/index',
                'topics/<slug:[A-Za-z0-9-]+>' => 'topics/view',
                'offers/' => 'offers/index',
                'offers/<slug:[A-Za-z0-9-]+>' => 'offers/view',
            ],
            'ignoreLanguageUrlPatterns' => [
                // route pattern => url pattern
                '#^catalog/get-filter#' => '#^catalog/get-filter#',
                '#^catalog/get-restaurants#' => '#^catalog/get-restaurants#',
                '#^order/get-available-times#' => '#^order/get-available-times#',
                '#^order/save#' => '#^order/save#',
                '#^user/user/login#' => '#^user/user/login#',
                '#^user/user/sign-up#' => '#^user/user/sign-up#',
                '#^set-city/<id:[0-9]+>#' => '#^set-city#',
            ],

        ],

    ],
    'params' => $params,
];
