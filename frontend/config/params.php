<?php
return [
    'articlesPerPage' => 20,
    'restaurantsPerPage' => 14,
    'newRestaurantsLimit' => 12,
    'popularRestaurantsLimit' => 12,
    'searchLimit' => 20,
    'specOffersMainPageLimit' => 8,
    'restaurantsOfWeekLimit' => 12,
    'restonRecomendedTopics' => 6,
    'newsAndReviewsMainPageLimit' => 10,
];
