<?php

namespace frontend\widgets\user;


use frontend\models\forms\SignUpForm;
use yii\base\Widget;

class SignUp extends Widget
{
    public function run()
    {
        $signUpForm = new SignUpForm();

        return $this->render('signup', [
            'signUpForm' => $signUpForm
        ]);
    }
}