<?php

namespace frontend\widgets\user;


use frontend\models\forms\LoginForm;

class UserAuth extends \yii\base\Widget
{
    public function run()
    {
        $loginForm = new LoginForm();

        return $this->render('login', [
            'loginForm' => $loginForm
        ]);
    }
}