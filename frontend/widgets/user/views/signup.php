<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

/** @var $signUpForm \frontend\models\forms\SignUpForm */
?>

<!-- Modal -->
<div id="registrationModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Для бронирования необходимо <b>зарегистрироваться</b> или <a href="#" class="login-modal-btn" data-toggle="modal" data-target="#loginModal">войти</a>:</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6">
                        <h4 class="text-center">facebook</h4>
                    </div>
                    <div class="col-sm-6">
                        <?php $form = ActiveForm::begin(['action' => ['/user/user/sign-up'], 'options' => ['autocomplete' => 'off', 'class' => 'order-form']]); ?>

                        <?= $form->field($signUpForm, 'userName')->textInput()->label('Ваше имя и фамилия:') ?>
                        <?= $form->field($signUpForm, 'email')->textInput()->label('Email:') ?>
                        <?= $form->field($signUpForm, 'phone')->textInput()->label('Мобильный телефон:') ?>
                        <?= $form->field($signUpForm, 'password')->passwordInput()->label('Пароль:') ?>
                        <?= $form->field($signUpForm, 'password_repeat')->passwordInput()->label('Подтверждение пароля:') ?>


                        <div class="text-center">
                            <?= Html::button('Регистрация', ['type' => 'submit', 'class' => 'btn btn-info sign-up-btn']) ?>
                        </div>

                        <?php $form::end() ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>