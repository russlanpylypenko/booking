<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

/** @var $loginForm \frontend\models\forms\LoginForm */
?>

<!-- Modal -->
<div id="loginModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Для бронирования необходимо <b>войти</b> или <a href="#" data-toggle="modal" class="sign-up-modal-btn" data-target="#registrationModal">зарегистрироваться</a>:</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6">
                        <h4 class="text-center">facebook</h4>
                    </div>
                    <div class="col-sm-6">
                        <?php $form = ActiveForm::begin(['action' => ['#'], 'options' => ['autocomplete' => 'off', 'class' => 'login-form']]); ?>

                        <?= $form->field($loginForm, 'email')->textInput()->label('Email:') ?>
                        <?= $form->field($loginForm, 'password')->passwordInput()->label('Пароль:') ?>

                        <div class="text-center">
                            <?= Html::button('Вход', ['class' => 'btn btn-info login-btn']) ?>
                        </div>

                        <?php $form::end() ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>