<?php

namespace frontend\widgets\order;

use Yii;
use frontend\models\forms\OrderForm;
use yii\base\Widget;

class Order extends Widget
{

    public function run()
    {
        $orderForm = new OrderForm();

        if (!Yii::$app->user->isGuest) {
            $orderForm->customer_phone = "+" . Yii::$app->user->identity->profile->FPHONE;
            $orderForm->customer_name = Yii::$app->user->identity->profile->getName();
        }

        return $this->render('index', [
            'orderForm' => $orderForm
        ]);
    }

}