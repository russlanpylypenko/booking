<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use dosamigos\datepicker\DatePicker;
use borales\extensions\phoneInput\PhoneInput;

/** @var $orderForm \frontend\models\forms\OrderForm */
?>

<!-- Modal -->
<div id="orderModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Закажите лучший столик бесплатно:</h4>
            </div>
            <div class="modal-body">
                <?php $form = ActiveForm::begin(['action' => ['#'], 'options' => ['autocomplete' => 'off', 'class' => 'order-form']]); ?>
                <div class="row">
                    <div class="col-sm-6">
                        <?= $form->field($orderForm, 'date')->widget(
                            DatePicker::className(), [
                            // inline too, not bad
                            'inline' => false,
                            // modify template for custom rendering
                            'template' => '{input}{addon}',
                            'clientOptions' => [
                                'autoclose' => false,
                                'format' => 'yyyy-mm-dd',
                            ]

                        ])->label('Дата:'); ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($orderForm, 'customer_name')->dropDownList($orderForm->getPeopleCountList())->label('Выберите количество гостей:') ?>
                    </div>
                </div>

                <?= $form->field($orderForm, 'time')->hiddenInput(['id' => 'order-time-input'])->label('Время:') ?>
                <div class="time-wrapper">

                </div>
                <?= $form->field($orderForm, 'customer_name')->textInput()->label('Ваше имя и фамилия:') ?>

                <?= $form->field($orderForm, 'customer_phone')->widget(PhoneInput::className(), [
                    'jsOptions' => [
                        'preferredCountries' => ['ua'],
                    ]
                ])->label('Мобильный телефон:'); ?>
                <?= $form->field($orderForm, 'comment')->textarea(['rows' => 2])->label('Пожелание к брони:') ?>
                <div class="text-right">
                    <?= Html::button('Забронировать', ['class' => 'btn btn-success save-order-btn']) ?>
                </div>

                <?php $form::end() ?>
            </div>
        </div>

    </div>
</div>


<!-- Modal -->
<div id="orderSuccessModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Столик успешно забронирован!</h4>
            </div>
            <div class="modal-body">
                <p>Информация по Вашему резерву:</p>
                <p><b>Время и дата:</b> -- </p>
                <p><b>Количество гостей:</b> -- </p>
                <p><b>Место:</b> -- </p>
                <p><b>Имя и Фамилия:</b> -- </p>
                <p><b>Телефон:</b> -- </p>
            </div>
        </div>

    </div>
</div>