<?php

namespace frontend\widgets\search;

use Yii;
use yii\base\Widget;
use frontend\models\forms\SearchForm;

class Search extends Widget
{
    public function run()
    {
        $searchForm = new SearchForm();

        if(Yii::$app->request->isGet){
            $searchForm->q = Yii::$app->request->get('q');
        }

        return $this->render('index', [
            'searchForm' => $searchForm
        ]);
    }
}