<?php

use yii\bootstrap\ActiveForm;

?>
<?php $form = ActiveForm::begin(['layout' => 'inline', 'action' => ['/search'], 'method' => 'get', 'options' => ['autocomplete' => 'off']]); ?>
<?= $form->field($searchForm, 'q', [
    'inputTemplate' => '<div class="input-group">{input}<span class="input-group-btn">' .
        '<button class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button></span></div>',
])->textInput(['placeholder' => 'Поиск', 'size' => 48, 'name' => 'q']) ?>
<?php $form::end() ?>