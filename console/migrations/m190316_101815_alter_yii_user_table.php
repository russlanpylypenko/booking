<?php

use yii\db\Migration;

/**
 * Class m190316_101815_alter_yii_user_table
 */
class m190316_101815_alter_yii_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%yii_user}}', 'auth_key', $this->string(32)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%yii_user}}', 'auth_key');
    }

}
