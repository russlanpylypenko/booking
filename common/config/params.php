<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'key' => 'LUsEgETEXaTexEWA-VyHeMeDE5iqUKUNu-He4AZycUJa2eHUCa-VuTYLeXURAkU9e2a',
];
